﻿namespace PPMachine
{
    partial class FrmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.lab_title = new System.Windows.Forms.Label();
            this.panel_top = new System.Windows.Forms.Panel();
            this.lab_en_title = new System.Windows.Forms.Label();
            this.lab_sysTime = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lab_logo = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.p_line = new System.Windows.Forms.Panel();
            this.panel_left = new System.Windows.Forms.Panel();
            this.groupBox_SysInfo = new System.Windows.Forms.GroupBox();
            this.gb_infoList = new System.Windows.Forms.GroupBox();
            this.tabCtr_Left = new System.Windows.Forms.TabControl();
            this.tabPage01 = new System.Windows.Forms.TabPage();
            this.lv_workinfo = new System.Windows.Forms.ListView();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tabPage02 = new System.Windows.Forms.TabPage();
            this.btn_wkorder = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lab_txtdevice = new System.Windows.Forms.Label();
            this.lab_TaskID = new System.Windows.Forms.Label();
            this.led_work_status = new NationalInstruments.UI.WindowsForms.Led();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.lab_wkorderList = new System.Windows.Forms.Label();
            this.lab_deviceNo = new System.Windows.Forms.Label();
            this.led_status_server = new NationalInstruments.UI.WindowsForms.Led();
            this.lab_work_status = new System.Windows.Forms.Label();
            this.lab_status_server = new System.Windows.Forms.Label();
            this.configDevice = new System.Windows.Forms.LinkLabel();
            this.lab_hmi_setcuttingqty = new System.Windows.Forms.Label();
            this.btn_wkorder_downparams = new System.Windows.Forms.Button();
            this.panel_right = new System.Windows.Forms.Panel();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lab_taskstatus = new System.Windows.Forms.Label();
            this.led_task_status = new NationalInstruments.UI.WindowsForms.Led();
            this.lab_MaterLotNo = new System.Windows.Forms.Label();
            this.lab_CustomCode = new System.Windows.Forms.Label();
            this.lab_MaterQuantity = new System.Windows.Forms.Label();
            this.lab_EndDate = new System.Windows.Forms.Label();
            this.lab_StartDate = new System.Windows.Forms.Label();
            this.lab_StdManHour = new System.Windows.Forms.Label();
            this.lab_MaterNo = new System.Windows.Forms.Label();
            this.lab_CurrProcCode = new System.Windows.Forms.Label();
            this.lab_ProcID = new System.Windows.Forms.Label();
            this.lab_PlanTime = new System.Windows.Forms.Label();
            this.lab_PlanQuantity = new System.Windows.Forms.Label();
            this.lab_subTaskId = new System.Windows.Forms.Label();
            this.lab_ppWidth = new System.Windows.Forms.Label();
            this.lab_PPLength = new System.Windows.Forms.Label();
            this.gb_opration = new System.Windows.Forms.GroupBox();
            this.tab_Actions = new System.Windows.Forms.TabControl();
            this.tab_01 = new System.Windows.Forms.TabPage();
            this.btn_wkorder_newget = new System.Windows.Forms.Button();
            this.btn_wkorder_getParams = new System.Windows.Forms.Button();
            this.btn_wkorder_start = new System.Windows.Forms.Button();
            this.btn_wkorder_getted = new System.Windows.Forms.Button();
            this.btn_wkorder_finished = new System.Windows.Forms.Button();
            this.tab_02 = new System.Windows.Forms.TabPage();
            this.btn_GetMater = new System.Windows.Forms.Button();
            this.btn_SetMater = new System.Windows.Forms.Button();
            this.gb_workorder = new System.Windows.Forms.GroupBox();
            this.lab_gbl_length_MainMovingaxis_total = new System.Windows.Forms.Label();
            this.lab_Hmi_CuttingLength = new System.Windows.Forms.Label();
            this.lab_Qbo_Lamp = new System.Windows.Forms.Label();
            this.lab_PPtype = new System.Windows.Forms.Label();
            this.lab_TotalCuttingQty = new System.Windows.Forms.Label();
            this.lab_NowcuttingQty = new System.Windows.Forms.Label();
            this.lab_Hmi_Total_CrossCuttingQty = new System.Windows.Forms.Label();
            this.lab_Hmi_TotalCuttingQty = new System.Windows.Forms.Label();
            this.lab_Hmi_MovingSpeed = new System.Windows.Forms.Label();
            this.gb_exception = new System.Windows.Forms.GroupBox();
            this.txt_MsgExcep = new System.Windows.Forms.TextBox();
            this.gb_AGV = new System.Windows.Forms.GroupBox();
            this.gb_alarmInfo = new System.Windows.Forms.GroupBox();
            this.tb_layalarm = new System.Windows.Forms.TableLayoutPanel();
            this.panel_top.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_left.SuspendLayout();
            this.groupBox_SysInfo.SuspendLayout();
            this.gb_infoList.SuspendLayout();
            this.tabCtr_Left.SuspendLayout();
            this.tabPage01.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.tabPage02.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.led_work_status)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.led_status_server)).BeginInit();
            this.panel_right.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.led_task_status)).BeginInit();
            this.gb_opration.SuspendLayout();
            this.tab_Actions.SuspendLayout();
            this.tab_01.SuspendLayout();
            this.tab_02.SuspendLayout();
            this.gb_workorder.SuspendLayout();
            this.gb_exception.SuspendLayout();
            this.gb_alarmInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // lab_title
            // 
            this.lab_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lab_title.AutoSize = true;
            this.lab_title.Font = new System.Drawing.Font("华文中宋", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_title.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lab_title.Location = new System.Drawing.Point(437, 16);
            this.lab_title.Name = "lab_title";
            this.lab_title.Size = new System.Drawing.Size(166, 40);
            this.lab_title.TabIndex = 0;
            this.lab_title.Text = "PP裁切机";
            // 
            // panel_top
            // 
            this.panel_top.Controls.Add(this.lab_en_title);
            this.panel_top.Controls.Add(this.lab_sysTime);
            this.panel_top.Controls.Add(this.lab_title);
            this.panel_top.Controls.Add(this.panel1);
            this.panel_top.Controls.Add(this.p_line);
            this.panel_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_top.Location = new System.Drawing.Point(0, 0);
            this.panel_top.Name = "panel_top";
            this.panel_top.Size = new System.Drawing.Size(1018, 103);
            this.panel_top.TabIndex = 2;
            // 
            // lab_en_title
            // 
            this.lab_en_title.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lab_en_title.AutoSize = true;
            this.lab_en_title.Font = new System.Drawing.Font("Calibri", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_en_title.ForeColor = System.Drawing.Color.SteelBlue;
            this.lab_en_title.Location = new System.Drawing.Point(423, 60);
            this.lab_en_title.Name = "lab_en_title";
            this.lab_en_title.Size = new System.Drawing.Size(210, 17);
            this.lab_en_title.TabIndex = 2;
            this.lab_en_title.Text = "Automatic Prepreg Cutting Machine";
            // 
            // lab_sysTime
            // 
            this.lab_sysTime.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lab_sysTime.AutoSize = true;
            this.lab_sysTime.Font = new System.Drawing.Font("LcdD", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_sysTime.ForeColor = System.Drawing.Color.Purple;
            this.lab_sysTime.Location = new System.Drawing.Point(837, 62);
            this.lab_sysTime.Name = "lab_sysTime";
            this.lab_sysTime.Size = new System.Drawing.Size(77, 19);
            this.lab_sysTime.TabIndex = 1;
            this.lab_sysTime.Text = "2017-8-20";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.lab_logo);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 91);
            this.panel1.TabIndex = 7;
            // 
            // lab_logo
            // 
            this.lab_logo.AutoSize = true;
            this.lab_logo.BackColor = System.Drawing.Color.Transparent;
            this.lab_logo.Font = new System.Drawing.Font("微软雅黑", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_logo.ForeColor = System.Drawing.Color.Navy;
            this.lab_logo.Location = new System.Drawing.Point(91, 17);
            this.lab_logo.Name = "lab_logo";
            this.lab_logo.Size = new System.Drawing.Size(133, 38);
            this.lab_logo.TabIndex = 2;
            this.lab_logo.Text = "正业科技";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 13);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(73, 65);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(89, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "ZHENGYE TECHNOLOGY";
            // 
            // p_line
            // 
            this.p_line.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.p_line.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.p_line.Location = new System.Drawing.Point(0, 98);
            this.p_line.Name = "p_line";
            this.p_line.Size = new System.Drawing.Size(1018, 5);
            this.p_line.TabIndex = 6;
            // 
            // panel_left
            // 
            this.panel_left.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel_left.Controls.Add(this.groupBox_SysInfo);
            this.panel_left.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel_left.Location = new System.Drawing.Point(0, 109);
            this.panel_left.Name = "panel_left";
            this.panel_left.Size = new System.Drawing.Size(245, 624);
            this.panel_left.TabIndex = 3;
            // 
            // groupBox_SysInfo
            // 
            this.groupBox_SysInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_SysInfo.Controls.Add(this.gb_infoList);
            this.groupBox_SysInfo.Controls.Add(this.groupBox7);
            this.groupBox_SysInfo.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox_SysInfo.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_SysInfo.Location = new System.Drawing.Point(3, -1);
            this.groupBox_SysInfo.Name = "groupBox_SysInfo";
            this.groupBox_SysInfo.Padding = new System.Windows.Forms.Padding(10);
            this.groupBox_SysInfo.Size = new System.Drawing.Size(237, 623);
            this.groupBox_SysInfo.TabIndex = 3;
            this.groupBox_SysInfo.TabStop = false;
            this.groupBox_SysInfo.Text = "系统状态";
            // 
            // gb_infoList
            // 
            this.gb_infoList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gb_infoList.Controls.Add(this.tabCtr_Left);
            this.gb_infoList.Location = new System.Drawing.Point(5, 230);
            this.gb_infoList.Name = "gb_infoList";
            this.gb_infoList.Size = new System.Drawing.Size(227, 386);
            this.gb_infoList.TabIndex = 5;
            this.gb_infoList.TabStop = false;
            // 
            // tabCtr_Left
            // 
            this.tabCtr_Left.Controls.Add(this.tabPage01);
            this.tabCtr_Left.Controls.Add(this.tabPage02);
            this.tabCtr_Left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtr_Left.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabCtr_Left.Location = new System.Drawing.Point(3, 29);
            this.tabCtr_Left.Name = "tabCtr_Left";
            this.tabCtr_Left.SelectedIndex = 0;
            this.tabCtr_Left.Size = new System.Drawing.Size(221, 354);
            this.tabCtr_Left.TabIndex = 0;
            // 
            // tabPage01
            // 
            this.tabPage01.Controls.Add(this.lv_workinfo);
            this.tabPage01.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabPage01.Location = new System.Drawing.Point(4, 22);
            this.tabPage01.Name = "tabPage01";
            this.tabPage01.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage01.Size = new System.Drawing.Size(213, 328);
            this.tabPage01.TabIndex = 0;
            this.tabPage01.Text = "加工信息";
            this.tabPage01.UseVisualStyleBackColor = true;
            // 
            // lv_workinfo
            // 
            this.lv_workinfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lv_workinfo.ContextMenuStrip = this.contextMenuStrip1;
            this.lv_workinfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lv_workinfo.LargeImageList = this.imageList1;
            this.lv_workinfo.Location = new System.Drawing.Point(3, 3);
            this.lv_workinfo.Name = "lv_workinfo";
            this.lv_workinfo.Size = new System.Drawing.Size(207, 322);
            this.lv_workinfo.SmallImageList = this.imageList1;
            this.lv_workinfo.TabIndex = 0;
            this.lv_workinfo.UseCompatibleStateImageBehavior = false;
            this.lv_workinfo.View = System.Windows.Forms.View.List;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            this.contextMenuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.contextMenuStrip1_ItemClicked);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.toolStripMenuItem1.Text = "清空信息";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "actionIcons_003.png");
            this.imageList1.Images.SetKeyName(1, "actionIcons_004.png");
            this.imageList1.Images.SetKeyName(2, "actionIcons_005.png");
            this.imageList1.Images.SetKeyName(3, "actionIcons_006.png");
            this.imageList1.Images.SetKeyName(4, "actionIcons_007.png");
            this.imageList1.Images.SetKeyName(5, "actionIcons_008.png");
            this.imageList1.Images.SetKeyName(6, "actionIcons_009.png");
            this.imageList1.Images.SetKeyName(7, "actionIcons_012.png");
            this.imageList1.Images.SetKeyName(8, "actionIcons_016.png");
            // 
            // tabPage02
            // 
            this.tabPage02.Controls.Add(this.btn_wkorder);
            this.tabPage02.Location = new System.Drawing.Point(4, 22);
            this.tabPage02.Name = "tabPage02";
            this.tabPage02.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage02.Size = new System.Drawing.Size(213, 328);
            this.tabPage02.TabIndex = 1;
            this.tabPage02.Text = "其他";
            this.tabPage02.UseVisualStyleBackColor = true;
            // 
            // btn_wkorder
            // 
            this.btn_wkorder.Location = new System.Drawing.Point(11, 22);
            this.btn_wkorder.Name = "btn_wkorder";
            this.btn_wkorder.Size = new System.Drawing.Size(113, 23);
            this.btn_wkorder.TabIndex = 0;
            this.btn_wkorder.Text = "工单信息维护";
            this.btn_wkorder.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lab_txtdevice);
            this.groupBox7.Controls.Add(this.lab_TaskID);
            this.groupBox7.Controls.Add(this.led_work_status);
            this.groupBox7.Controls.Add(this.btn_stop);
            this.groupBox7.Controls.Add(this.btn_start);
            this.groupBox7.Controls.Add(this.lab_wkorderList);
            this.groupBox7.Controls.Add(this.lab_deviceNo);
            this.groupBox7.Controls.Add(this.led_status_server);
            this.groupBox7.Controls.Add(this.lab_work_status);
            this.groupBox7.Controls.Add(this.lab_status_server);
            this.groupBox7.Controls.Add(this.configDevice);
            this.groupBox7.Location = new System.Drawing.Point(5, 28);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(227, 196);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            // 
            // lab_txtdevice
            // 
            this.lab_txtdevice.AutoSize = true;
            this.lab_txtdevice.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_txtdevice.Location = new System.Drawing.Point(87, 53);
            this.lab_txtdevice.Name = "lab_txtdevice";
            this.lab_txtdevice.Size = new System.Drawing.Size(69, 20);
            this.lab_txtdevice.TabIndex = 6;
            this.lab_txtdevice.Text = "======";
            // 
            // lab_TaskID
            // 
            this.lab_TaskID.AutoSize = true;
            this.lab_TaskID.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_TaskID.Location = new System.Drawing.Point(87, 85);
            this.lab_TaskID.Name = "lab_TaskID";
            this.lab_TaskID.Size = new System.Drawing.Size(69, 20);
            this.lab_TaskID.TabIndex = 6;
            this.lab_TaskID.Text = "======";
            // 
            // led_work_status
            // 
            this.led_work_status.LedStyle = NationalInstruments.UI.LedStyle.Square3D;
            this.led_work_status.Location = new System.Drawing.Point(84, 113);
            this.led_work_status.Name = "led_work_status";
            this.led_work_status.OffColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.led_work_status.Size = new System.Drawing.Size(127, 36);
            this.led_work_status.TabIndex = 5;
            // 
            // btn_stop
            // 
            this.btn_stop.FlatAppearance.BorderSize = 0;
            this.btn_stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_stop.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_stop.Image = global::PPMachine.Properties.Resources.btn_bg_gray;
            this.btn_stop.Location = new System.Drawing.Point(130, 158);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(61, 29);
            this.btn_stop.TabIndex = 4;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_start
            // 
            this.btn_start.FlatAppearance.BorderSize = 0;
            this.btn_start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_start.Font = new System.Drawing.Font("Georgia", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_start.Image = global::PPMachine.Properties.Resources.btn_bg;
            this.btn_start.Location = new System.Drawing.Point(46, 158);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(59, 29);
            this.btn_start.TabIndex = 4;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = false;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // lab_wkorderList
            // 
            this.lab_wkorderList.AutoSize = true;
            this.lab_wkorderList.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_wkorderList.ForeColor = System.Drawing.Color.Black;
            this.lab_wkorderList.Location = new System.Drawing.Point(16, 91);
            this.lab_wkorderList.Name = "lab_wkorderList";
            this.lab_wkorderList.Size = new System.Drawing.Size(53, 12);
            this.lab_wkorderList.TabIndex = 3;
            this.lab_wkorderList.Text = "工单号：";
            // 
            // lab_deviceNo
            // 
            this.lab_deviceNo.AutoSize = true;
            this.lab_deviceNo.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_deviceNo.ForeColor = System.Drawing.Color.Black;
            this.lab_deviceNo.Location = new System.Drawing.Point(16, 58);
            this.lab_deviceNo.Name = "lab_deviceNo";
            this.lab_deviceNo.Size = new System.Drawing.Size(65, 12);
            this.lab_deviceNo.TabIndex = 3;
            this.lab_deviceNo.Text = "设备编号：";
            // 
            // led_status_server
            // 
            this.led_status_server.LedStyle = NationalInstruments.UI.LedStyle.Round3D;
            this.led_status_server.Location = new System.Drawing.Point(84, 18);
            this.led_status_server.Name = "led_status_server";
            this.led_status_server.Size = new System.Drawing.Size(49, 28);
            this.led_status_server.TabIndex = 0;
            // 
            // lab_work_status
            // 
            this.lab_work_status.AutoSize = true;
            this.lab_work_status.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_work_status.ForeColor = System.Drawing.Color.Black;
            this.lab_work_status.Location = new System.Drawing.Point(16, 124);
            this.lab_work_status.Name = "lab_work_status";
            this.lab_work_status.Size = new System.Drawing.Size(65, 12);
            this.lab_work_status.TabIndex = 2;
            this.lab_work_status.Text = "加工状态：";
            // 
            // lab_status_server
            // 
            this.lab_status_server.AutoSize = true;
            this.lab_status_server.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_status_server.ForeColor = System.Drawing.Color.Black;
            this.lab_status_server.Location = new System.Drawing.Point(16, 25);
            this.lab_status_server.Name = "lab_status_server";
            this.lab_status_server.Size = new System.Drawing.Size(53, 12);
            this.lab_status_server.TabIndex = 2;
            this.lab_status_server.Text = "服务器：";
            // 
            // configDevice
            // 
            this.configDevice.AutoSize = true;
            this.configDevice.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.configDevice.Location = new System.Drawing.Point(155, 25);
            this.configDevice.Name = "configDevice";
            this.configDevice.Size = new System.Drawing.Size(63, 14);
            this.configDevice.TabIndex = 1;
            this.configDevice.TabStop = true;
            this.configDevice.Text = "配置连接";
            this.configDevice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.configDevice_LinkClicked);
            // 
            // lab_hmi_setcuttingqty
            // 
            this.lab_hmi_setcuttingqty.AutoSize = true;
            this.lab_hmi_setcuttingqty.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_hmi_setcuttingqty.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_hmi_setcuttingqty.Location = new System.Drawing.Point(9, 91);
            this.lab_hmi_setcuttingqty.Name = "lab_hmi_setcuttingqty";
            this.lab_hmi_setcuttingqty.Size = new System.Drawing.Size(94, 19);
            this.lab_hmi_setcuttingqty.TabIndex = 0;
            this.lab_hmi_setcuttingqty.Text = "裁切次数:";
            // 
            // btn_wkorder_downparams
            // 
            this.btn_wkorder_downparams.FlatAppearance.BorderSize = 0;
            this.btn_wkorder_downparams.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_wkorder_downparams.ForeColor = System.Drawing.Color.Black;
            this.btn_wkorder_downparams.Location = new System.Drawing.Point(127, 34);
            this.btn_wkorder_downparams.Name = "btn_wkorder_downparams";
            this.btn_wkorder_downparams.Size = new System.Drawing.Size(83, 23);
            this.btn_wkorder_downparams.TabIndex = 1;
            this.btn_wkorder_downparams.Text = "4.下发参数";
            this.btn_wkorder_downparams.UseVisualStyleBackColor = true;
            this.btn_wkorder_downparams.Click += new System.EventHandler(this.btn_set_params_Click);
            // 
            // panel_right
            // 
            this.panel_right.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_right.Controls.Add(this.groupBox5);
            this.panel_right.Location = new System.Drawing.Point(246, 109);
            this.panel_right.Name = "panel_right";
            this.panel_right.Size = new System.Drawing.Size(769, 624);
            this.panel_right.TabIndex = 4;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.groupBox2);
            this.groupBox5.Controls.Add(this.gb_opration);
            this.groupBox5.Controls.Add(this.gb_workorder);
            this.groupBox5.Controls.Add(this.gb_exception);
            this.groupBox5.Controls.Add(this.gb_AGV);
            this.groupBox5.Controls.Add(this.gb_alarmInfo);
            this.groupBox5.Font = new System.Drawing.Font("微软雅黑", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox5.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox5.Location = new System.Drawing.Point(0, -1);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox5.Size = new System.Drawing.Size(766, 623);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "工作状态";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.groupBox2.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox2.Location = new System.Drawing.Point(9, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(502, 270);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "派工单信息";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.panel2.Controls.Add(this.lab_taskstatus);
            this.panel2.Controls.Add(this.led_task_status);
            this.panel2.Controls.Add(this.lab_MaterLotNo);
            this.panel2.Controls.Add(this.lab_CustomCode);
            this.panel2.Controls.Add(this.lab_MaterQuantity);
            this.panel2.Controls.Add(this.lab_EndDate);
            this.panel2.Controls.Add(this.lab_StartDate);
            this.panel2.Controls.Add(this.lab_StdManHour);
            this.panel2.Controls.Add(this.lab_MaterNo);
            this.panel2.Controls.Add(this.lab_CurrProcCode);
            this.panel2.Controls.Add(this.lab_ProcID);
            this.panel2.Controls.Add(this.lab_PlanTime);
            this.panel2.Controls.Add(this.lab_PlanQuantity);
            this.panel2.Controls.Add(this.lab_subTaskId);
            this.panel2.Controls.Add(this.lab_ppWidth);
            this.panel2.Controls.Add(this.lab_PPLength);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 22);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(496, 245);
            this.panel2.TabIndex = 4;
            // 
            // lab_taskstatus
            // 
            this.lab_taskstatus.AutoSize = true;
            this.lab_taskstatus.Font = new System.Drawing.Font("Consolas", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_taskstatus.Location = new System.Drawing.Point(310, 8);
            this.lab_taskstatus.Name = "lab_taskstatus";
            this.lab_taskstatus.Size = new System.Drawing.Size(56, 17);
            this.lab_taskstatus.TabIndex = 5;
            this.lab_taskstatus.Text = "未领取";
            // 
            // led_task_status
            // 
            this.led_task_status.BlinkMode = NationalInstruments.UI.LedBlinkMode.BlinkWhenOff;
            this.led_task_status.LedStyle = NationalInstruments.UI.LedStyle.Square3D;
            this.led_task_status.Location = new System.Drawing.Point(245, 2);
            this.led_task_status.Name = "led_task_status";
            this.led_task_status.Size = new System.Drawing.Size(59, 30);
            this.led_task_status.TabIndex = 4;
            // 
            // lab_MaterLotNo
            // 
            this.lab_MaterLotNo.AutoSize = true;
            this.lab_MaterLotNo.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_MaterLotNo.ForeColor = System.Drawing.Color.Crimson;
            this.lab_MaterLotNo.Location = new System.Drawing.Point(18, 182);
            this.lab_MaterLotNo.Name = "lab_MaterLotNo";
            this.lab_MaterLotNo.Size = new System.Drawing.Size(123, 19);
            this.lab_MaterLotNo.TabIndex = 3;
            this.lab_MaterLotNo.Text = "物料批次号：";
            this.lab_MaterLotNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_CustomCode
            // 
            this.lab_CustomCode.AutoSize = true;
            this.lab_CustomCode.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_CustomCode.ForeColor = System.Drawing.Color.Crimson;
            this.lab_CustomCode.Location = new System.Drawing.Point(18, 153);
            this.lab_CustomCode.Name = "lab_CustomCode";
            this.lab_CustomCode.Size = new System.Drawing.Size(104, 19);
            this.lab_CustomCode.TabIndex = 3;
            this.lab_CustomCode.Text = "客户料号：";
            this.lab_CustomCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_MaterQuantity
            // 
            this.lab_MaterQuantity.AutoSize = true;
            this.lab_MaterQuantity.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_MaterQuantity.ForeColor = System.Drawing.Color.Crimson;
            this.lab_MaterQuantity.Location = new System.Drawing.Point(18, 211);
            this.lab_MaterQuantity.Name = "lab_MaterQuantity";
            this.lab_MaterQuantity.Size = new System.Drawing.Size(104, 19);
            this.lab_MaterQuantity.TabIndex = 3;
            this.lab_MaterQuantity.Text = "物料数量：";
            this.lab_MaterQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_EndDate
            // 
            this.lab_EndDate.AutoSize = true;
            this.lab_EndDate.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_EndDate.ForeColor = System.Drawing.Color.Crimson;
            this.lab_EndDate.Location = new System.Drawing.Point(241, 126);
            this.lab_EndDate.Name = "lab_EndDate";
            this.lab_EndDate.Size = new System.Drawing.Size(142, 19);
            this.lab_EndDate.TabIndex = 3;
            this.lab_EndDate.Text = "计划结束时间：";
            this.lab_EndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_StartDate
            // 
            this.lab_StartDate.AutoSize = true;
            this.lab_StartDate.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_StartDate.ForeColor = System.Drawing.Color.Crimson;
            this.lab_StartDate.Location = new System.Drawing.Point(241, 97);
            this.lab_StartDate.Name = "lab_StartDate";
            this.lab_StartDate.Size = new System.Drawing.Size(142, 19);
            this.lab_StartDate.TabIndex = 3;
            this.lab_StartDate.Text = "计划开始时间：";
            this.lab_StartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_StdManHour
            // 
            this.lab_StdManHour.AutoSize = true;
            this.lab_StdManHour.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_StdManHour.ForeColor = System.Drawing.Color.Crimson;
            this.lab_StdManHour.Location = new System.Drawing.Point(241, 68);
            this.lab_StdManHour.Name = "lab_StdManHour";
            this.lab_StdManHour.Size = new System.Drawing.Size(104, 19);
            this.lab_StdManHour.TabIndex = 3;
            this.lab_StdManHour.Text = "标准工时：";
            this.lab_StdManHour.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_MaterNo
            // 
            this.lab_MaterNo.AutoSize = true;
            this.lab_MaterNo.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_MaterNo.ForeColor = System.Drawing.Color.Crimson;
            this.lab_MaterNo.Location = new System.Drawing.Point(241, 39);
            this.lab_MaterNo.Name = "lab_MaterNo";
            this.lab_MaterNo.Size = new System.Drawing.Size(84, 19);
            this.lab_MaterNo.TabIndex = 3;
            this.lab_MaterNo.Text = "PP料号：";
            this.lab_MaterNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_CurrProcCode
            // 
            this.lab_CurrProcCode.AutoSize = true;
            this.lab_CurrProcCode.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_CurrProcCode.ForeColor = System.Drawing.Color.Crimson;
            this.lab_CurrProcCode.Location = new System.Drawing.Point(18, 124);
            this.lab_CurrProcCode.Name = "lab_CurrProcCode";
            this.lab_CurrProcCode.Size = new System.Drawing.Size(142, 19);
            this.lab_CurrProcCode.TabIndex = 3;
            this.lab_CurrProcCode.Text = "当前工序编号：";
            this.lab_CurrProcCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_ProcID
            // 
            this.lab_ProcID.AutoSize = true;
            this.lab_ProcID.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_ProcID.ForeColor = System.Drawing.Color.Crimson;
            this.lab_ProcID.Location = new System.Drawing.Point(18, 95);
            this.lab_ProcID.Name = "lab_ProcID";
            this.lab_ProcID.Size = new System.Drawing.Size(122, 19);
            this.lab_ProcID.TabIndex = 3;
            this.lab_ProcID.Text = "当前工序ID：";
            this.lab_ProcID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_PlanTime
            // 
            this.lab_PlanTime.AutoSize = true;
            this.lab_PlanTime.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_PlanTime.ForeColor = System.Drawing.Color.Crimson;
            this.lab_PlanTime.Location = new System.Drawing.Point(18, 66);
            this.lab_PlanTime.Name = "lab_PlanTime";
            this.lab_PlanTime.Size = new System.Drawing.Size(161, 19);
            this.lab_PlanTime.TabIndex = 3;
            this.lab_PlanTime.Text = "计划需求总工时：";
            this.lab_PlanTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_PlanQuantity
            // 
            this.lab_PlanQuantity.AutoSize = true;
            this.lab_PlanQuantity.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_PlanQuantity.ForeColor = System.Drawing.Color.Crimson;
            this.lab_PlanQuantity.Location = new System.Drawing.Point(18, 37);
            this.lab_PlanQuantity.Name = "lab_PlanQuantity";
            this.lab_PlanQuantity.Size = new System.Drawing.Size(104, 19);
            this.lab_PlanQuantity.TabIndex = 3;
            this.lab_PlanQuantity.Text = "计划产量：";
            this.lab_PlanQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_subTaskId
            // 
            this.lab_subTaskId.AutoSize = true;
            this.lab_subTaskId.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_subTaskId.ForeColor = System.Drawing.Color.Crimson;
            this.lab_subTaskId.Location = new System.Drawing.Point(18, 8);
            this.lab_subTaskId.Name = "lab_subTaskId";
            this.lab_subTaskId.Size = new System.Drawing.Size(85, 19);
            this.lab_subTaskId.TabIndex = 3;
            this.lab_subTaskId.Text = "派工单：";
            this.lab_subTaskId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_ppWidth
            // 
            this.lab_ppWidth.AutoSize = true;
            this.lab_ppWidth.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_ppWidth.ForeColor = System.Drawing.Color.Crimson;
            this.lab_ppWidth.Location = new System.Drawing.Point(241, 184);
            this.lab_ppWidth.Name = "lab_ppWidth";
            this.lab_ppWidth.Size = new System.Drawing.Size(94, 19);
            this.lab_ppWidth.TabIndex = 0;
            this.lab_ppWidth.Text = "裁切宽度:";
            // 
            // lab_PPLength
            // 
            this.lab_PPLength.AutoSize = true;
            this.lab_PPLength.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_PPLength.ForeColor = System.Drawing.Color.Crimson;
            this.lab_PPLength.Location = new System.Drawing.Point(241, 155);
            this.lab_PPLength.Name = "lab_PPLength";
            this.lab_PPLength.Size = new System.Drawing.Size(94, 19);
            this.lab_PPLength.TabIndex = 0;
            this.lab_PPLength.Text = "裁切长度:";
            // 
            // gb_opration
            // 
            this.gb_opration.Controls.Add(this.tab_Actions);
            this.gb_opration.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.gb_opration.ForeColor = System.Drawing.Color.Maroon;
            this.gb_opration.Location = new System.Drawing.Point(516, 25);
            this.gb_opration.Name = "gb_opration";
            this.gb_opration.Size = new System.Drawing.Size(244, 270);
            this.gb_opration.TabIndex = 10;
            this.gb_opration.TabStop = false;
            this.gb_opration.Text = "功能按钮";
            // 
            // tab_Actions
            // 
            this.tab_Actions.Controls.Add(this.tab_01);
            this.tab_Actions.Controls.Add(this.tab_02);
            this.tab_Actions.Location = new System.Drawing.Point(6, 21);
            this.tab_Actions.Name = "tab_Actions";
            this.tab_Actions.SelectedIndex = 0;
            this.tab_Actions.Size = new System.Drawing.Size(232, 243);
            this.tab_Actions.TabIndex = 2;
            // 
            // tab_01
            // 
            this.tab_01.Controls.Add(this.btn_wkorder_newget);
            this.tab_01.Controls.Add(this.btn_wkorder_getParams);
            this.tab_01.Controls.Add(this.btn_wkorder_start);
            this.tab_01.Controls.Add(this.btn_wkorder_getted);
            this.tab_01.Controls.Add(this.btn_wkorder_downparams);
            this.tab_01.Controls.Add(this.btn_wkorder_finished);
            this.tab_01.Location = new System.Drawing.Point(4, 29);
            this.tab_01.Name = "tab_01";
            this.tab_01.Padding = new System.Windows.Forms.Padding(3);
            this.tab_01.Size = new System.Drawing.Size(224, 210);
            this.tab_01.TabIndex = 0;
            this.tab_01.Text = "工单";
            this.tab_01.UseVisualStyleBackColor = true;
            // 
            // btn_wkorder_newget
            // 
            this.btn_wkorder_newget.FlatAppearance.BorderSize = 0;
            this.btn_wkorder_newget.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_wkorder_newget.ForeColor = System.Drawing.Color.Black;
            this.btn_wkorder_newget.Location = new System.Drawing.Point(127, 126);
            this.btn_wkorder_newget.Name = "btn_wkorder_newget";
            this.btn_wkorder_newget.Size = new System.Drawing.Size(83, 23);
            this.btn_wkorder_newget.TabIndex = 1;
            this.btn_wkorder_newget.Text = "6.重新获取";
            this.btn_wkorder_newget.UseVisualStyleBackColor = true;
            this.btn_wkorder_newget.Click += new System.EventHandler(this.btn_wkorder_newget_Click);
            // 
            // btn_wkorder_getParams
            // 
            this.btn_wkorder_getParams.FlatAppearance.BorderSize = 0;
            this.btn_wkorder_getParams.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_wkorder_getParams.ForeColor = System.Drawing.Color.Black;
            this.btn_wkorder_getParams.Location = new System.Drawing.Point(15, 80);
            this.btn_wkorder_getParams.Name = "btn_wkorder_getParams";
            this.btn_wkorder_getParams.Size = new System.Drawing.Size(83, 23);
            this.btn_wkorder_getParams.TabIndex = 1;
            this.btn_wkorder_getParams.Text = "2.获取参数";
            this.btn_wkorder_getParams.UseVisualStyleBackColor = true;
            this.btn_wkorder_getParams.Click += new System.EventHandler(this.btn_wkorder_getParams_Click);
            // 
            // btn_wkorder_start
            // 
            this.btn_wkorder_start.FlatAppearance.BorderSize = 0;
            this.btn_wkorder_start.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_wkorder_start.ForeColor = System.Drawing.Color.Black;
            this.btn_wkorder_start.Location = new System.Drawing.Point(15, 128);
            this.btn_wkorder_start.Name = "btn_wkorder_start";
            this.btn_wkorder_start.Size = new System.Drawing.Size(83, 23);
            this.btn_wkorder_start.TabIndex = 1;
            this.btn_wkorder_start.Text = "3.已开始";
            this.btn_wkorder_start.UseVisualStyleBackColor = true;
            this.btn_wkorder_start.Click += new System.EventHandler(this.btn_wkorder_start_Click);
            // 
            // btn_wkorder_getted
            // 
            this.btn_wkorder_getted.FlatAppearance.BorderSize = 0;
            this.btn_wkorder_getted.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_wkorder_getted.ForeColor = System.Drawing.Color.Black;
            this.btn_wkorder_getted.Location = new System.Drawing.Point(15, 32);
            this.btn_wkorder_getted.Name = "btn_wkorder_getted";
            this.btn_wkorder_getted.Size = new System.Drawing.Size(83, 23);
            this.btn_wkorder_getted.TabIndex = 1;
            this.btn_wkorder_getted.Text = "1.已领取";
            this.btn_wkorder_getted.UseVisualStyleBackColor = true;
            this.btn_wkorder_getted.Click += new System.EventHandler(this.btn_wkorder_getted_Click);
            // 
            // btn_wkorder_finished
            // 
            this.btn_wkorder_finished.FlatAppearance.BorderSize = 0;
            this.btn_wkorder_finished.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_wkorder_finished.ForeColor = System.Drawing.Color.Black;
            this.btn_wkorder_finished.Location = new System.Drawing.Point(127, 80);
            this.btn_wkorder_finished.Name = "btn_wkorder_finished";
            this.btn_wkorder_finished.Size = new System.Drawing.Size(83, 23);
            this.btn_wkorder_finished.TabIndex = 1;
            this.btn_wkorder_finished.Text = "5.已完成";
            this.btn_wkorder_finished.UseVisualStyleBackColor = true;
            this.btn_wkorder_finished.Click += new System.EventHandler(this.btn_wkorder_finished_Click);
            // 
            // tab_02
            // 
            this.tab_02.Controls.Add(this.btn_GetMater);
            this.tab_02.Controls.Add(this.btn_SetMater);
            this.tab_02.Location = new System.Drawing.Point(4, 29);
            this.tab_02.Name = "tab_02";
            this.tab_02.Padding = new System.Windows.Forms.Padding(3);
            this.tab_02.Size = new System.Drawing.Size(224, 210);
            this.tab_02.TabIndex = 1;
            this.tab_02.Text = "物料";
            this.tab_02.UseVisualStyleBackColor = true;
            // 
            // btn_GetMater
            // 
            this.btn_GetMater.FlatAppearance.BorderSize = 0;
            this.btn_GetMater.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_GetMater.ForeColor = System.Drawing.Color.Black;
            this.btn_GetMater.Location = new System.Drawing.Point(17, 11);
            this.btn_GetMater.Name = "btn_GetMater";
            this.btn_GetMater.Size = new System.Drawing.Size(83, 23);
            this.btn_GetMater.TabIndex = 3;
            this.btn_GetMater.Text = "上料";
            this.btn_GetMater.UseVisualStyleBackColor = true;
            this.btn_GetMater.Click += new System.EventHandler(this.btn_GetMater_Click);
            // 
            // btn_SetMater
            // 
            this.btn_SetMater.FlatAppearance.BorderSize = 0;
            this.btn_SetMater.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btn_SetMater.ForeColor = System.Drawing.Color.Black;
            this.btn_SetMater.Location = new System.Drawing.Point(126, 11);
            this.btn_SetMater.Name = "btn_SetMater";
            this.btn_SetMater.Size = new System.Drawing.Size(83, 23);
            this.btn_SetMater.TabIndex = 2;
            this.btn_SetMater.Text = "下料";
            this.btn_SetMater.UseVisualStyleBackColor = true;
            // 
            // gb_workorder
            // 
            this.gb_workorder.Controls.Add(this.lab_hmi_setcuttingqty);
            this.gb_workorder.Controls.Add(this.lab_gbl_length_MainMovingaxis_total);
            this.gb_workorder.Controls.Add(this.lab_Hmi_CuttingLength);
            this.gb_workorder.Controls.Add(this.lab_Qbo_Lamp);
            this.gb_workorder.Controls.Add(this.lab_PPtype);
            this.gb_workorder.Controls.Add(this.lab_TotalCuttingQty);
            this.gb_workorder.Controls.Add(this.lab_NowcuttingQty);
            this.gb_workorder.Controls.Add(this.lab_Hmi_Total_CrossCuttingQty);
            this.gb_workorder.Controls.Add(this.lab_Hmi_TotalCuttingQty);
            this.gb_workorder.Controls.Add(this.lab_Hmi_MovingSpeed);
            this.gb_workorder.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.gb_workorder.ForeColor = System.Drawing.Color.Maroon;
            this.gb_workorder.Location = new System.Drawing.Point(11, 292);
            this.gb_workorder.Name = "gb_workorder";
            this.gb_workorder.Size = new System.Drawing.Size(502, 184);
            this.gb_workorder.TabIndex = 4;
            this.gb_workorder.TabStop = false;
            this.gb_workorder.Text = "机器信息";
            // 
            // lab_gbl_length_MainMovingaxis_total
            // 
            this.lab_gbl_length_MainMovingaxis_total.AutoSize = true;
            this.lab_gbl_length_MainMovingaxis_total.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_gbl_length_MainMovingaxis_total.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_gbl_length_MainMovingaxis_total.Location = new System.Drawing.Point(219, 33);
            this.lab_gbl_length_MainMovingaxis_total.Name = "lab_gbl_length_MainMovingaxis_total";
            this.lab_gbl_length_MainMovingaxis_total.Size = new System.Drawing.Size(151, 19);
            this.lab_gbl_length_MainMovingaxis_total.TabIndex = 0;
            this.lab_gbl_length_MainMovingaxis_total.Text = "当前总送料米数:";
            // 
            // lab_Hmi_CuttingLength
            // 
            this.lab_Hmi_CuttingLength.AutoSize = true;
            this.lab_Hmi_CuttingLength.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_Hmi_CuttingLength.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_Hmi_CuttingLength.Location = new System.Drawing.Point(9, 120);
            this.lab_Hmi_CuttingLength.Name = "lab_Hmi_CuttingLength";
            this.lab_Hmi_CuttingLength.Size = new System.Drawing.Size(94, 19);
            this.lab_Hmi_CuttingLength.TabIndex = 0;
            this.lab_Hmi_CuttingLength.Text = "裁切长度:";
            // 
            // lab_Qbo_Lamp
            // 
            this.lab_Qbo_Lamp.AutoSize = true;
            this.lab_Qbo_Lamp.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_Qbo_Lamp.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_Qbo_Lamp.Location = new System.Drawing.Point(9, 33);
            this.lab_Qbo_Lamp.Name = "lab_Qbo_Lamp";
            this.lab_Qbo_Lamp.Size = new System.Drawing.Size(142, 19);
            this.lab_Qbo_Lamp.TabIndex = 0;
            this.lab_Qbo_Lamp.Text = "机器运行状态：";
            // 
            // lab_PPtype
            // 
            this.lab_PPtype.AutoSize = true;
            this.lab_PPtype.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_PPtype.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_PPtype.Location = new System.Drawing.Point(9, 62);
            this.lab_PPtype.Name = "lab_PPtype";
            this.lab_PPtype.Size = new System.Drawing.Size(74, 19);
            this.lab_PPtype.TabIndex = 0;
            this.lab_PPtype.Text = "PP料号:";
            // 
            // lab_TotalCuttingQty
            // 
            this.lab_TotalCuttingQty.AutoSize = true;
            this.lab_TotalCuttingQty.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_TotalCuttingQty.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_TotalCuttingQty.Location = new System.Drawing.Point(219, 93);
            this.lab_TotalCuttingQty.Name = "lab_TotalCuttingQty";
            this.lab_TotalCuttingQty.Size = new System.Drawing.Size(151, 19);
            this.lab_TotalCuttingQty.TabIndex = 0;
            this.lab_TotalCuttingQty.Text = "当前总裁切次数:";
            // 
            // lab_NowcuttingQty
            // 
            this.lab_NowcuttingQty.AutoSize = true;
            this.lab_NowcuttingQty.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_NowcuttingQty.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_NowcuttingQty.Location = new System.Drawing.Point(219, 63);
            this.lab_NowcuttingQty.Name = "lab_NowcuttingQty";
            this.lab_NowcuttingQty.Size = new System.Drawing.Size(132, 19);
            this.lab_NowcuttingQty.TabIndex = 0;
            this.lab_NowcuttingQty.Text = "当前裁切次数:";
            // 
            // lab_Hmi_Total_CrossCuttingQty
            // 
            this.lab_Hmi_Total_CrossCuttingQty.AutoSize = true;
            this.lab_Hmi_Total_CrossCuttingQty.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_Hmi_Total_CrossCuttingQty.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_Hmi_Total_CrossCuttingQty.Location = new System.Drawing.Point(219, 153);
            this.lab_Hmi_Total_CrossCuttingQty.Name = "lab_Hmi_Total_CrossCuttingQty";
            this.lab_Hmi_Total_CrossCuttingQty.Size = new System.Drawing.Size(189, 19);
            this.lab_Hmi_Total_CrossCuttingQty.TabIndex = 0;
            this.lab_Hmi_Total_CrossCuttingQty.Text = "纵切刀限制米数设定:";
            // 
            // lab_Hmi_TotalCuttingQty
            // 
            this.lab_Hmi_TotalCuttingQty.AutoSize = true;
            this.lab_Hmi_TotalCuttingQty.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_Hmi_TotalCuttingQty.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_Hmi_TotalCuttingQty.Location = new System.Drawing.Point(219, 123);
            this.lab_Hmi_TotalCuttingQty.Name = "lab_Hmi_TotalCuttingQty";
            this.lab_Hmi_TotalCuttingQty.Size = new System.Drawing.Size(189, 19);
            this.lab_Hmi_TotalCuttingQty.TabIndex = 0;
            this.lab_Hmi_TotalCuttingQty.Text = "横切刀限制次数设定:";
            this.lab_Hmi_TotalCuttingQty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_Hmi_MovingSpeed
            // 
            this.lab_Hmi_MovingSpeed.AutoSize = true;
            this.lab_Hmi_MovingSpeed.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lab_Hmi_MovingSpeed.ForeColor = System.Drawing.Color.DarkBlue;
            this.lab_Hmi_MovingSpeed.Location = new System.Drawing.Point(9, 149);
            this.lab_Hmi_MovingSpeed.Name = "lab_Hmi_MovingSpeed";
            this.lab_Hmi_MovingSpeed.Size = new System.Drawing.Size(94, 19);
            this.lab_Hmi_MovingSpeed.TabIndex = 0;
            this.lab_Hmi_MovingSpeed.Text = "送料速度:";
            // 
            // gb_exception
            // 
            this.gb_exception.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gb_exception.Controls.Add(this.txt_MsgExcep);
            this.gb_exception.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.gb_exception.ForeColor = System.Drawing.Color.Maroon;
            this.gb_exception.Location = new System.Drawing.Point(516, 475);
            this.gb_exception.Name = "gb_exception";
            this.gb_exception.Size = new System.Drawing.Size(244, 141);
            this.gb_exception.TabIndex = 9;
            this.gb_exception.TabStop = false;
            this.gb_exception.Text = "异常信息";
            // 
            // txt_MsgExcep
            // 
            this.txt_MsgExcep.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_MsgExcep.Location = new System.Drawing.Point(3, 22);
            this.txt_MsgExcep.Multiline = true;
            this.txt_MsgExcep.Name = "txt_MsgExcep";
            this.txt_MsgExcep.Size = new System.Drawing.Size(238, 116);
            this.txt_MsgExcep.TabIndex = 0;
            // 
            // gb_AGV
            // 
            this.gb_AGV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gb_AGV.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.gb_AGV.ForeColor = System.Drawing.Color.Maroon;
            this.gb_AGV.Location = new System.Drawing.Point(9, 475);
            this.gb_AGV.Name = "gb_AGV";
            this.gb_AGV.Size = new System.Drawing.Size(501, 141);
            this.gb_AGV.TabIndex = 8;
            this.gb_AGV.TabStop = false;
            this.gb_AGV.Text = "AGV小车";
            // 
            // gb_alarmInfo
            // 
            this.gb_alarmInfo.Controls.Add(this.tb_layalarm);
            this.gb_alarmInfo.Font = new System.Drawing.Font("微软雅黑", 10.5F);
            this.gb_alarmInfo.ForeColor = System.Drawing.Color.Maroon;
            this.gb_alarmInfo.Location = new System.Drawing.Point(516, 292);
            this.gb_alarmInfo.Name = "gb_alarmInfo";
            this.gb_alarmInfo.Size = new System.Drawing.Size(244, 184);
            this.gb_alarmInfo.TabIndex = 7;
            this.gb_alarmInfo.TabStop = false;
            this.gb_alarmInfo.Text = "当前报警";
            // 
            // tb_layalarm
            // 
            this.tb_layalarm.ColumnCount = 3;
            this.tb_layalarm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33332F));
            this.tb_layalarm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tb_layalarm.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tb_layalarm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tb_layalarm.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tb_layalarm.Location = new System.Drawing.Point(3, 22);
            this.tb_layalarm.Name = "tb_layalarm";
            this.tb_layalarm.RowCount = 4;
            this.tb_layalarm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tb_layalarm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tb_layalarm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tb_layalarm.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tb_layalarm.Size = new System.Drawing.Size(238, 159);
            this.tb_layalarm.TabIndex = 0;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1018, 740);
            this.Controls.Add(this.panel_right);
            this.Controls.Add(this.panel_left);
            this.Controls.Add(this.panel_top);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PP裁切机";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMain_FormClosing);
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FrmMain_KeyDown);
            this.panel_top.ResumeLayout(false);
            this.panel_top.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_left.ResumeLayout(false);
            this.groupBox_SysInfo.ResumeLayout(false);
            this.gb_infoList.ResumeLayout(false);
            this.tabCtr_Left.ResumeLayout(false);
            this.tabPage01.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.tabPage02.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.led_work_status)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.led_status_server)).EndInit();
            this.panel_right.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.led_task_status)).EndInit();
            this.gb_opration.ResumeLayout(false);
            this.tab_Actions.ResumeLayout(false);
            this.tab_01.ResumeLayout(false);
            this.tab_02.ResumeLayout(false);
            this.gb_workorder.ResumeLayout(false);
            this.gb_workorder.PerformLayout();
            this.gb_exception.ResumeLayout(false);
            this.gb_exception.PerformLayout();
            this.gb_alarmInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lab_title;
        private System.Windows.Forms.Panel panel_top;
        private System.Windows.Forms.Panel panel_left;
        private System.Windows.Forms.Panel panel_right;
        private NationalInstruments.UI.WindowsForms.Led led_status_server;
        private System.Windows.Forms.LinkLabel configDevice;
        private System.Windows.Forms.GroupBox groupBox_SysInfo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lab_logo;
        private System.Windows.Forms.Label lab_status_server;
        private System.Windows.Forms.Label lab_deviceNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Panel p_line;
        private System.Windows.Forms.Label lab_NowcuttingQty;
        private System.Windows.Forms.GroupBox gb_infoList;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Button btn_stop;
        private NationalInstruments.UI.WindowsForms.Led led_work_status;
        private System.Windows.Forms.ListView lv_workinfo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lab_sysTime;
        private System.Windows.Forms.Label lab_Hmi_MovingSpeed;
        private System.Windows.Forms.Label lab_work_status;
        private System.Windows.Forms.Button btn_wkorder_downparams;
        private System.Windows.Forms.Label lab_hmi_setcuttingqty;
        private System.Windows.Forms.TabControl tabCtr_Left;
        private System.Windows.Forms.TabPage tabPage01;
        private System.Windows.Forms.TabPage tabPage02;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.GroupBox gb_workorder;
        private System.Windows.Forms.GroupBox gb_alarmInfo;
        private System.Windows.Forms.TableLayoutPanel tb_layalarm;
        private System.Windows.Forms.Button btn_wkorder_getParams;
        private System.Windows.Forms.GroupBox gb_AGV;
        private System.Windows.Forms.GroupBox gb_exception;
        private System.Windows.Forms.Label lab_wkorderList;
        private System.Windows.Forms.Label lab_gbl_length_MainMovingaxis_total;
        private System.Windows.Forms.Label lab_Hmi_CuttingLength;
        private System.Windows.Forms.Label lab_PPtype;
        private System.Windows.Forms.Label lab_Hmi_TotalCuttingQty;
        private System.Windows.Forms.Label lab_Hmi_Total_CrossCuttingQty;
        private System.Windows.Forms.Label lab_Qbo_Lamp;
        private System.Windows.Forms.TextBox txt_MsgExcep;
        private System.Windows.Forms.Label lab_TotalCuttingQty;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.Label lab_en_title;
        private System.Windows.Forms.GroupBox gb_opration;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lab_MaterNo;
        private System.Windows.Forms.Label lab_ppWidth;
        private System.Windows.Forms.Label lab_PPLength;
        private System.Windows.Forms.Button btn_wkorder_start;
        private System.Windows.Forms.Button btn_wkorder_newget;
        private System.Windows.Forms.Button btn_wkorder_finished;
        private System.Windows.Forms.Label lab_subTaskId;
        private System.Windows.Forms.Button btn_wkorder;
        private System.Windows.Forms.TabControl tab_Actions;
        private System.Windows.Forms.TabPage tab_01;
        private System.Windows.Forms.TabPage tab_02;
        private System.Windows.Forms.Button btn_GetMater;
        private System.Windows.Forms.Button btn_SetMater;
        private System.Windows.Forms.Label lab_TaskID;
        private System.Windows.Forms.Label lab_txtdevice;
        private System.Windows.Forms.Label lab_PlanTime;
        private System.Windows.Forms.Label lab_PlanQuantity;
        private System.Windows.Forms.Label lab_ProcID;
        private System.Windows.Forms.Label lab_CurrProcCode;
        private System.Windows.Forms.Label lab_CustomCode;
        private System.Windows.Forms.Label lab_MaterLotNo;
        private System.Windows.Forms.Label lab_MaterQuantity;
        private System.Windows.Forms.Label lab_StdManHour;
        private System.Windows.Forms.Label lab_EndDate;
        private System.Windows.Forms.Label lab_StartDate;
        private System.Windows.Forms.Button btn_wkorder_getted;
        private NationalInstruments.UI.WindowsForms.Led led_task_status;
        private System.Windows.Forms.Label lab_taskstatus;

    }
}

