﻿namespace PPMachine
{
    partial class Frm_ReadWritePPM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataSocket1 = new NationalInstruments.Net.DataSocket(this.components);
            this.txt_read_url = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_write_url = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_write_val = new System.Windows.Forms.TextBox();
            this.btn_read = new System.Windows.Forms.Button();
            this.btn_write = new System.Windows.Forms.Button();
            this.lab_readResult = new System.Windows.Forms.Label();
            this.txt_connect_msg = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataSocket1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataSocket1
            // 
            this.dataSocket1.ConnectionStatusUpdated += new NationalInstruments.Net.ConnectionStatusUpdatedEventHandler(this.dataSocket1_ConnectionStatusUpdated);
            this.dataSocket1.DataUpdated += new NationalInstruments.Net.DataUpdatedEventHandler(this.dataSocket1_DataUpdated);
            // 
            // txt_read_url
            // 
            this.txt_read_url.Location = new System.Drawing.Point(153, 61);
            this.txt_read_url.Multiline = true;
            this.txt_read_url.Name = "txt_read_url";
            this.txt_read_url.Size = new System.Drawing.Size(233, 42);
            this.txt_read_url.TabIndex = 0;
            this.txt_read_url.Text = "opc://localhost/OPC.SimaticNET/D425.VarProg.gbo_nowcuttingqty";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(77, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "读取地址：";
            // 
            // txt_write_url
            // 
            this.txt_write_url.Location = new System.Drawing.Point(153, 168);
            this.txt_write_url.Multiline = true;
            this.txt_write_url.Name = "txt_write_url";
            this.txt_write_url.Size = new System.Drawing.Size(233, 46);
            this.txt_write_url.TabIndex = 0;
            this.txt_write_url.Text = "opc://localhost/OPC.SimaticNET/D425.VarProg.hmi_setcuttingqty";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(77, 171);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "写入地址：";
            // 
            // txt_write_val
            // 
            this.txt_write_val.Location = new System.Drawing.Point(153, 237);
            this.txt_write_val.Name = "txt_write_val";
            this.txt_write_val.Size = new System.Drawing.Size(100, 21);
            this.txt_write_val.TabIndex = 2;
            this.txt_write_val.Text = "8";
            // 
            // btn_read
            // 
            this.btn_read.Location = new System.Drawing.Point(439, 66);
            this.btn_read.Name = "btn_read";
            this.btn_read.Size = new System.Drawing.Size(75, 23);
            this.btn_read.TabIndex = 3;
            this.btn_read.Text = "读取";
            this.btn_read.UseVisualStyleBackColor = true;
            this.btn_read.Click += new System.EventHandler(this.btn_read_Click);
            // 
            // btn_write
            // 
            this.btn_write.Location = new System.Drawing.Point(439, 254);
            this.btn_write.Name = "btn_write";
            this.btn_write.Size = new System.Drawing.Size(75, 23);
            this.btn_write.TabIndex = 4;
            this.btn_write.Text = "写入";
            this.btn_write.UseVisualStyleBackColor = true;
            this.btn_write.Click += new System.EventHandler(this.btn_write_Click);
            // 
            // lab_readResult
            // 
            this.lab_readResult.AutoSize = true;
            this.lab_readResult.Location = new System.Drawing.Point(192, 121);
            this.lab_readResult.Name = "lab_readResult";
            this.lab_readResult.Size = new System.Drawing.Size(89, 12);
            this.lab_readResult.TabIndex = 5;
            this.lab_readResult.Text = "lab_readResult";
            // 
            // txt_connect_msg
            // 
            this.txt_connect_msg.Location = new System.Drawing.Point(153, 283);
            this.txt_connect_msg.Multiline = true;
            this.txt_connect_msg.Name = "txt_connect_msg";
            this.txt_connect_msg.Size = new System.Drawing.Size(248, 85);
            this.txt_connect_msg.TabIndex = 6;
            // 
            // Frm_ReadWritePPM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 395);
            this.Controls.Add(this.txt_connect_msg);
            this.Controls.Add(this.lab_readResult);
            this.Controls.Add(this.btn_write);
            this.Controls.Add(this.btn_read);
            this.Controls.Add(this.txt_write_val);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_write_url);
            this.Controls.Add(this.txt_read_url);
            this.Name = "Frm_ReadWritePPM";
            this.Text = "Frm_ReadWritePPM";
            ((System.ComponentModel.ISupportInitialize)(this.dataSocket1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private NationalInstruments.Net.DataSocket dataSocket1;
        private System.Windows.Forms.TextBox txt_read_url;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_write_url;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_write_val;
        private System.Windows.Forms.Button btn_read;
        private System.Windows.Forms.Button btn_write;
        private System.Windows.Forms.Label lab_readResult;
        private System.Windows.Forms.TextBox txt_connect_msg;
    }
}