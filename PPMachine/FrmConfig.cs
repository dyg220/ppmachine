﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using WcsApi;
using NationalInstruments.Net;
using System.Threading;

namespace PPMachine
{
    public partial class FrmConfig : Form
    {
        public FrmConfig()
        {
            InitializeComponent();
        }

        public bool isOK { set; get; }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            string device_NO = txt_DeviceNO.Text.Trim();
            string address_IP = txt_Address.Text.Trim();
            // string address_OPC = txt_OPC.Text.Trim();

            if (string.IsNullOrEmpty(device_NO))
            {
                MessageBox.Show("请设定设备号");
                return;
            }
            if (string.IsNullOrEmpty(address_IP))
            {
                MessageBox.Show("请设定服务器IP");
                return;
            }
            //if (string.IsNullOrEmpty(address_OPC))
            //{
            //    MessageBox.Show("请设定OPC地址");
            //    return;
            //}
            else
            {
                try
                {
                    PPDevice.DevicNO = device_NO;
                    //PPDevice.OPC = address_OPC;

                    WCSApi.SetKeyWord(device_NO);
                    WCSApi.Connect(address_IP);

                    if (WCSApi.IsConnected())
                    {
                        //WCSApi.Get += FrmMain.MessageRecieve;
                        //WCSApi.ShowNotice += FrmMain.NoticeRecieve;
                        WCSApi.SendRegister(); //先注册
                        Thread.Sleep(1000);
                        WCSApi.SendApply();  //再验证
                        Thread.Sleep(1000);
                        if (WCSApi.IsSynced() && WCSApi.IsEncrypted())
                        {
                            PPDevice.IsConnectServer = true;
                            MessageBox.Show("服务器连接成功");
                        }

                    }

                    else
                    {
                        MessageBox.Show("服务器连接失败");
                        PPDevice.IsConnectServer = false;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("服务器连接失败:" + ex.Message);
                    PPDevice.IsConnectServer = false;
                }


                //PPDevice.Socket.Connect(address_OPC);
                //if (PPDevice.Socket.IsConnected)
                //{
                //    PPDevice.IsConnectOPC = true;
                //    MessageBox.Show("OPC连接成功");
                //}
                //elsek
                //{
                //    PPDevice.IsConnectOPC = false;
                //    MessageBox.Show("OPC连接失败");
                //}

                isOK = true;
                this.Close();

            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
