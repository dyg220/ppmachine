﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NationalInstruments.Net;

namespace PPMachine
{
    public partial class Frm_ReadWritePPM : Form
    {
        public Frm_ReadWritePPM()
        {
            InitializeComponent();
        }

        private void btn_read_Click(object sender, EventArgs e)
        {
            string read_url = txt_read_url.Text;

            dataSocket1.Connect(read_url, AccessMode.Read);

            if (dataSocket1.IsConnected)
            {
                lab_readResult.Text = dataSocket1.Data.Value.ToString();

            }

        }

        private void dataSocket1_DataUpdated(object sender, DataUpdatedEventArgs e)
        {


        }

        private void btn_write_Click(object sender, EventArgs e)
        {
            string w_url = txt_write_url.Text;

            dataSocket1.Connect(w_url, AccessMode.Write);
            if (dataSocket1.IsConnected)
            {
                dataSocket1.Data.Value = txt_write_val.Text;
                //dataSocket1.Data.Attributes["Value"].Value = txt_write_val.Text;
                //dataSocket1.DataUpdated += new DataUpdatedEventHandler(dataSocket1_DataUpdated);
                dataSocket1.Update();
            }


        }

        private void dataSocket1_ConnectionStatusUpdated(object sender, ConnectionStatusUpdatedEventArgs e)
        {
            txt_connect_msg.Text = e.Message;
        }
    }
}
