﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NationalInstruments.UI;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Threading.Tasks;
using NationalInstruments.Net;
using System.Xml.Linq;
using System.Diagnostics;
using WcsApi;
using WCS.API;
using System.Web.Script.Serialization;
using System.Net;
using NationalInstruments.UI.WindowsForms;
using PPMachine.Common;

namespace PPMachine
{
    public partial class FrmMain : Form
    {
        Stopwatch sw = new Stopwatch();
        PPDevice.SocketUrl sockect_url;
        XElement element = XmlHelper.Load("Targets.xml");


        List<DataSocket> s_read = new List<DataSocket>(11);  //参数数据
        List<DataSocket> ds_alarm = new List<DataSocket>(12); //报警数据


        DataSocketData rs_data = new DataSocketData();

        System.Windows.Forms.Timer frm_timer = new System.Windows.Forms.Timer(); //时间显示
        System.Timers.Timer plc_get_timer;  //OPC读取定时器
        //System.Timers.Timer downLoad_timer;  //Socket服务器读取定时器
        System.Timers.Timer upLoad_timer; //Socket服务器上传定时器

        public delegate void DisplayUI(string content, int count);

        public delegate void UpDateUI(WorkOrder model, string buscode);

        public delegate void AddMessageUI(string content, string tp);

        string[] alarms = new string[] { "横切刀", "前送料辊", "气胀轴", "模组", "横切灯罩", "系统启动未完全", "无料", "冷水机", "堵料", "横切刀口危险", "横切到达总裁切次数", "纵切到达总裁切米数" };
        bool[] alarm_vals = new bool[12];

        // private object threadLock = new object();


        public FrmMain()
        {
            InitializeComponent();

        }
        private static object LockObject = new Object();


        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            FrmConfig config = new FrmConfig();
            config.ShowDialog();

        }

        private void timerCall(object sender)
        {
            //this.Invoke(new System.Action(() =>
            //{
            //    // progressBar_right.
            //    progressBar_right.Value = (int)(sw.Elapsed.Seconds);
            //    progressBar_right.Step = 1000;

            //}));


            //sw.Stop();


        }


        private void FrmMain_Load(object sender, EventArgs e)
        {

            SetPanelWidth();
            for (int i = 0; i < 11; i++)
            {
                s_read.Add(new DataSocket());
            }
            for (int i = 0; i < 12; i++)
            {
                ds_alarm.Add(new DataSocket());
            }

            lab_sysTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            S_ReadsetParams(element);
            SetAlarmInfo(element);

            frm_timer.Interval = 1000;
            frm_timer.Tick += new EventHandler(timer_Tick);
            frm_timer.Enabled = true;
            frm_timer.Start();


        }

        private void S_ReadsetParams(XElement element)
        {

            //纵切刀限制米数设定
            XElement target1 = XmlHelper.QueryElements(element, "target", "name", "hmi_total_crosscuttingqty").FirstOrDefault();
            sockect_url.Url_Hmi_Total_CrossCuttingQty = XmlHelper.QueryElement(target1, "url").Value;
            s_read[0].Connect(sockect_url.Url_Hmi_Total_CrossCuttingQty, AccessMode.ReadAutoUpdate);

            //横切刀限制次数设定
            XElement target2 = XmlHelper.QueryElements(element, "target", "name", "hmi_totalcuttingqty").FirstOrDefault();
            sockect_url.Url_Hmi_TotalCuttingQty = XmlHelper.QueryElement(target2, "url").Value;
            s_read[1].Connect(sockect_url.Url_Hmi_TotalCuttingQty, AccessMode.ReadAutoUpdate);


            //送料速度
            XElement target3 = XmlHelper.QueryElements(element, "target", "name", "hmi_movingspeed").FirstOrDefault();
            sockect_url.Url_Hmi_MovingSpeed = XmlHelper.QueryElement(target3, "url").Value;
            s_read[2].Connect(sockect_url.Url_Hmi_MovingSpeed, AccessMode.ReadAutoUpdate);
            //s4.Connect(sockect_url.Url_Hmi_SetCuttingQty, AccessMode.WriteAutoUpdate);

            //当前裁切次数
            XElement target4 = XmlHelper.QueryElements(element, "target", "name", "gbo_nowcuttingqty").FirstOrDefault();
            sockect_url.Url_NowcuttingQty = XmlHelper.QueryElement(target4, "url").Value;
            s_read[3].Connect(sockect_url.Url_NowcuttingQty, AccessMode.ReadAutoUpdate);

            //当前总裁切次数
            XElement target5 = XmlHelper.QueryElements(element, "target", "name", "gbo_totalcuttingqty").FirstOrDefault();
            sockect_url.Url_TotalCuttingQty = XmlHelper.QueryElement(target5, "url").Value;
            s_read[4].Connect(sockect_url.Url_TotalCuttingQty, AccessMode.ReadAutoUpdate);


            //当前总送料米数
            XElement target6 = XmlHelper.QueryElements(element, "target", "name", "gbl_length_MainMovingaxis_total_m").FirstOrDefault();
            sockect_url.Url_Length_MainMovingaxis_Total = XmlHelper.QueryElement(target6, "url").Value;
            s_read[5].Connect(sockect_url.Url_Length_MainMovingaxis_Total, AccessMode.ReadAutoUpdate);

            //PP料号
            XElement target7 = XmlHelper.QueryElements(element, "target", "name", "hmi_PPtype").FirstOrDefault();
            sockect_url.Url_Hmi_PPtype = XmlHelper.QueryElement(target7, "url").Value;
            s_read[6].Connect(sockect_url.Url_Hmi_PPtype, AccessMode.ReadAutoUpdate);
            s_read[10].Connect(sockect_url.Url_Hmi_PPtype, AccessMode.WriteAutoUpdate); //写PP料号


            //裁切次数
            XElement target8 = XmlHelper.QueryElements(element, "target", "name", "hmi_setcuttingqty").FirstOrDefault();
            sockect_url.Url_Hmi_SetCuttingQty = XmlHelper.QueryElement(target8, "url").Value;
            s_read[7].Connect(sockect_url.Url_Hmi_SetCuttingQty, AccessMode.ReadAutoUpdate);

            //裁切长度
            XElement target9 = XmlHelper.QueryElements(element, "target", "name", "hmi_cuttinglength").FirstOrDefault();
            sockect_url.Url_Hmi_CuttingLength = XmlHelper.QueryElement(target9, "url").Value;
            s_read[8].Connect(sockect_url.Url_Hmi_CuttingLength, AccessMode.ReadAutoUpdate);

            //机器运行状态
            XElement target10 = XmlHelper.QueryElements(element, "target", "name", "qbo_Lamp").FirstOrDefault();
            sockect_url.Url_Qbo_Lamp = XmlHelper.QueryElement(target10, "url").Value;
            s_read[9].Connect(sockect_url.Url_Qbo_Lamp, AccessMode.ReadAutoUpdate);


            rs_data.Attributes.Add("Hmi_Total_CrossCuttingQty", "");  //纵切刀限制米数设定
            rs_data.Attributes.Add("Hmi_TotalCuttingQty", ""); //横切刀限制次数设定
            rs_data.Attributes.Add("Hmi_MovingSpeed", "");  //送料速度
            rs_data.Attributes.Add("NowcuttingQty", "");  //当前裁切次数
            rs_data.Attributes.Add("TotalCuttingQty", "");  //当前总裁切次数
            rs_data.Attributes.Add("Length_MainMovingaxis_Total", ""); //当前总送料米数
            rs_data.Attributes.Add("Hmi_PPtype", "");  //PP料号
            rs_data.Attributes.Add("Hmi_SetCuttingQty", "");  //裁切次数
            rs_data.Attributes.Add("Hmi_CuttingLength", "");  //裁切长度
            // data.Attributes.Add("Hmi_CuttingWidth", "");  //裁切宽度,socket服务器获取
            rs_data.Attributes.Add("Qbo_Lamp", ""); //机器运行状态
            rs_data.Attributes.Add("Send_OK", ""); //Send_OK


        }

        private void SetAlarmInfo(XElement element)
        {
            for (int i = 0; i < alarms.Length; i++)
            {
                AlarmControl ac = new AlarmControl();
                ac.Name = "ac" + i.ToString();
                //ac.led1.Size = new System.Drawing.Size(16, 18);
                //ac.lab_title.Font = new System.Drawing.Font("Consolas", 9);
                ac.lab_title.Text = alarms[i];
                ac.led1.Value = alarm_vals[i];
                tb_layalarm.Controls.Add(ac);
            }

            //横切刀报警
            XElement target1 = XmlHelper.QueryElements(element, "target", "name", "gbo_zonalknife_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_ZonalKnife_Alarm_Doing = XmlHelper.QueryElement(target1, "url").Value;
            ds_alarm[0].Connect(sockect_url.Url_gbo_ZonalKnife_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //前送料辊报警
            XElement target2 = XmlHelper.QueryElements(element, "target", "name", "gbo_FrontRoller_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_FrontRoller_Alarm_Doing = XmlHelper.QueryElement(target2, "url").Value;
            ds_alarm[1].Connect(sockect_url.Url_gbo_FrontRoller_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //气胀轴报警
            XElement target3 = XmlHelper.QueryElements(element, "target", "name", "gbo_AirRoller_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_AirRoller_Alarm_Doing = XmlHelper.QueryElement(target3, "url").Value;
            ds_alarm[2].Connect(sockect_url.Url_gbo_AirRoller_Alarm_Doing, AccessMode.ReadAutoUpdate);


            //模组报警
            XElement target4 = XmlHelper.QueryElements(element, "target", "name", "gbo_RightModule_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_RightModule_Alarm_Doing = XmlHelper.QueryElement(target4, "url").Value;
            ds_alarm[3].Connect(sockect_url.Url_gbo_RightModule_Alarm_Doing, AccessMode.ReadAutoUpdate);


            //横切灯罩报警
            XElement target5 = XmlHelper.QueryElements(element, "target", "name", "gbo_ZonalHeatter_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_ZonalHeatter_Alarm_Doing = XmlHelper.QueryElement(target5, "url").Value;
            ds_alarm[4].Connect(sockect_url.Url_gbo_ZonalHeatter_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //系统启动未完全报警
            XElement target6 = XmlHelper.QueryElements(element, "target", "name", "gbo_start_sign_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_Start_Sign_Alarm_Doing = XmlHelper.QueryElement(target6, "url").Value;
            ds_alarm[5].Connect(sockect_url.Url_gbo_Start_Sign_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //无料报警
            XElement target7 = XmlHelper.QueryElements(element, "target", "name", "gbo_NoPP_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_NoPP_Alarm_Doing = XmlHelper.QueryElement(target7, "url").Value;
            ds_alarm[6].Connect(sockect_url.Url_gbo_NoPP_Alarm_Doing, AccessMode.ReadAutoUpdate);


            //冷水机报警
            XElement target8 = XmlHelper.QueryElements(element, "target", "name", "gbo_Cooler_Alarm").FirstOrDefault();
            sockect_url.Url_gbo_Cooler_Alarm = XmlHelper.QueryElement(target8, "url").Value;
            ds_alarm[7].Connect(sockect_url.Url_gbo_Cooler_Alarm, AccessMode.ReadAutoUpdate);


            //堵料报警
            XElement target9 = XmlHelper.QueryElements(element, "target", "name", "gbo_PPError_Alarm").FirstOrDefault();
            sockect_url.Url_gbo_PPError_Alarm = XmlHelper.QueryElement(target9, "url").Value;
            ds_alarm[8].Connect(sockect_url.Url_gbo_PPError_Alarm, AccessMode.ReadAutoUpdate);

            //横切刀口危险报警
            XElement target10 = XmlHelper.QueryElements(element, "target", "name", "gbo_ZonalCutter_Alarm").FirstOrDefault();
            sockect_url.Url_gbo_ZonalCutter_Alarm = XmlHelper.QueryElement(target10, "url").Value;
            ds_alarm[9].Connect(sockect_url.Url_gbo_ZonalCutter_Alarm, AccessMode.ReadAutoUpdate);

            //横切到达总裁切次数报警
            XElement target11 = XmlHelper.QueryElements(element, "target", "name", "gbo_totalcuttingqty_alarm").FirstOrDefault();
            sockect_url.Url_gbo_TotalCuttingQty_Alarm = XmlHelper.QueryElement(target11, "url").Value;
            ds_alarm[10].Connect(sockect_url.Url_gbo_TotalCuttingQty_Alarm, AccessMode.ReadAutoUpdate);

            //纵切到达总裁切米数报警
            XElement target12 = XmlHelper.QueryElements(element, "target", "name", "gbo_total_crosscuttingqty_alarm").FirstOrDefault();
            sockect_url.Url_gbo_Total_CrossCuttingQty_Alarm = XmlHelper.QueryElement(target12, "url").Value;
            ds_alarm[11].Connect(sockect_url.Url_gbo_Total_CrossCuttingQty_Alarm, AccessMode.ReadAutoUpdate);



        }
        Random r = new Random();
        static JavaScriptSerializer _serial = new JavaScriptSerializer();
        private void GetWorkOrder()
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {

                #region 构造iMesbus协议
                var model = new IMesbus();
                model.Code = "CPS1001"; //请求工单
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();
                newdic.Add("LineCode", PPDevice.DevicNO);  //cps1001

                //newdic.Add("SubTaskID", "14031");  // SubTaskID":"14031","TaskID":"0000000130", cps1002
                //newdic.Add("CompleteQuantity", "72");  //cps1003

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));

                #endregion
            }


        }
        public void NoticeRecieve(string message)
        {

        }
        WorkOrder wk_order = new WorkOrder();
        public void MessageRecieve(string message)
        {
            IMesbus bus = (IMesbus)_serial.Deserialize(message, typeof(IMesbus));
            if (bus.Data.Code == "Success")
            {
                UpDateUI update = new UpDateUI(showMsg);
                AddMessageUI addmessage = new AddMessageUI(AddMeassage);
                if (bus.Code.Equals("CPS1001"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.TaskID = obj["TaskID"].ToString();
                    wk_order.SubTaskID = obj["SubTaskID"].ToString();
                    wk_order.CustomCode = obj["CustomCode"].ToString(); //1080
                    wk_order.PlanQuantity = int.Parse(obj["PlanQuantity"].ToString());
                    wk_order.PlanTime = float.Parse(obj["PlanTime"].ToString());
                    wk_order.LineCode = obj["LineCode"].ToString();
                    wk_order.ProcID = int.Parse(obj["ProcID"].ToString());
                    wk_order.CurrProcCode = obj["CurrProcCode"].ToString();
                    wk_order.MaterNo = obj["MaterNo"].ToString();

                    lab_TaskID.Invoke(update, new object[] { wk_order, bus.Code });
                    lab_MaterNo.Invoke(update, new object[] { wk_order, bus.Code });
                    lv_workinfo.Invoke(addmessage, new object[] { "获取工单信息", "01" });

                }
                if (bus.Code.Equals("CPS1002"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        TaskSatus.isGet = true;
                        lab_taskstatus.Invoke(addmessage, new object[] { "已领取", "02" });
                    }
                }
                if (bus.Code.Equals("CPS1003"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);

                }
                if (bus.Code.Equals("CPS1004"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        TaskSatus.isStart = true;
                        lab_taskstatus.Invoke(addmessage, new object[] { "已开始", "03" });
                    }
                }

                if (bus.Code.Equals("CPS1005"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        TaskSatus.isFinish = true;
                        lab_taskstatus.Invoke(addmessage, new object[] { "已完成", "04" });
                    }
                }


            }
            else if (bus.Data.Code == "Error")
            {
                MessageBox.Show(bus.Data.Returns.ToString());
            }

        }

        public void AddMeassage(string content, string tp)
        {

            switch (tp)
            {
                case "01":
                    if (lv_workinfo.IsHandleCreated)
                    {
                        ListViewItem lvitem = new ListViewItem();
                        lvitem.ImageIndex = 7;
                        lvitem.SubItems[0].Text = content;
                        lv_workinfo.Items.Add(lvitem);
                        //Cmb_wkorderList.Items.Add(wk_order.TaskID);
                        //lab_TaskID.Text = wk_order.TaskID;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lv_workinfo.Invoke(update, new object[] { content, tp });
                    }
                    break;
                case "02":
                    if (lab_taskstatus.IsHandleCreated)
                    {
                        ListViewItem lvitem = new ListViewItem();
                        lvitem.ImageIndex = 7;
                        lvitem.SubItems[0].Text = "工单已领取";
                        lv_workinfo.Items.Add(lvitem);
                        lab_taskstatus.Text = content;
                        led_task_status.Value = true;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_taskstatus.Invoke(update, new object[] { content, tp });
                    }
                    break;
                case "03":
                    if (lab_taskstatus.IsHandleCreated)
                    {
                        ListViewItem lvitem = new ListViewItem();
                        lvitem.ImageIndex = 7;
                        lvitem.SubItems[0].Text = "任务已开始";
                        lv_workinfo.Items.Add(lvitem);
                        lab_taskstatus.Text = content;
                        led_task_status.Value = true;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_taskstatus.Invoke(update, new object[] { content, tp });
                    }
                    break;
                case "04":
                    if (lab_taskstatus.IsHandleCreated)
                    {
                        ListViewItem lvitem = new ListViewItem();
                        lvitem.ImageIndex = 7;
                        lvitem.SubItems[0].Text = "更新任务已完成";
                        lv_workinfo.Items.Add(lvitem);
                        lab_taskstatus.Text = content;
                        led_task_status.Value = true;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_taskstatus.Invoke(update, new object[] { content, tp });
                    }
                    break;
                default:
                    break;
            }


        }

        public void showMsg(WorkOrder model, string buscode)
        {
            switch (buscode)
            {
                case "CPS1001":
                    if (lab_TaskID.IsHandleCreated)
                    {
                        lab_TaskID.Text = model.TaskID;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_TaskID.Invoke(update, new object[] { model, buscode });
                    }
                    if (lab_MaterNo.IsHandleCreated)
                    {
                        lab_subTaskId.Text = "派工单：" + model.SubTaskID;
                        lab_PlanQuantity.Text = "计划产量：" + model.PlanQuantity;
                        lab_PlanTime.Text = "计划需求总工时：" + model.PlanTime.ToString("f2");
                        lab_ProcID.Text = "当前工序ID：" + model.ProcID;
                        lab_CurrProcCode.Text = "当前工序编号：" + model.CurrProcCode;
                        lab_CustomCode.Text = "客户料号：" + model.CustomCode;
                        lab_MaterLotNo.Text = "物料批次号：" + model.MaterLotNo;
                        lab_MaterQuantity.Text = "物料数量：" + model.MaterQuantity;
                        lab_StdManHour.Text = "标准工时：" + model.StdManHour;
                        lab_StartDate.Text = "计划开始时间：" + model.StartDate.ToString("HH:ss:mm");
                        lab_EndDate.Text = "计划结束时间：" + model.EndDate.ToString("HH:ss:mm");
                        lab_MaterNo.Text = "PP料号：" + model.MaterNo;
                        lab_PPLength.Text = "裁切长度:";
                        lab_ppWidth.Text = "裁切宽度：";
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_MaterNo.Invoke(update, new object[] { model, buscode });
                    }
                    break;
                default:
                    break;
            }



        }
        void tc_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //更新参数
            UpdateReadsetParmas();

            //更新报警
            UpdateAlarms();

        }

        private void UpdateAlarms()
        {
            for (int i = 0; i < ds_alarm.Count; i++)
            {
                if (ds_alarm[i].IsConnected)
                {
                    if (ds_alarm[i].SyncRead(500))
                    {
                        alarm_vals[i] = (bool)ds_alarm[i].Data.Value;
                        AlarmControl ac = tb_layalarm.Controls.Find("ac" + i.ToString(), true).FirstOrDefault() as AlarmControl;
                        if (ac.IsHandleCreated)
                        {
                            ac.led1.Value = alarm_vals[i];
                        }
                        else
                        {
                            ac.Invoke(new System.Action(() =>
                            {
                                ac.led1.Value = alarm_vals[i];
                            }));
                        }
                    }

                }
            }

        }

        private void UpdateReadsetParmas()
        {
            if (s_read[0].IsConnected)
            {
                if (s_read[0].SyncRead(500))
                {
                    //纵切刀限制米数设定
                    rs_data.Attributes["Hmi_Total_CrossCuttingQty"].Value = s_read[0].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_Hmi_Total_CrossCuttingQty.Invoke(dui, new object[] { "纵切刀限制米数设定:" + "\t" + rs_data.Attributes["Hmi_Total_CrossCuttingQty"].Value, 1 });
                }

            }
            if (s_read[1].IsConnected)
            {
                if (s_read[1].SyncRead(500))
                {
                    //横切刀限制次数设定
                    rs_data.Attributes["Hmi_TotalCuttingQty"].Value = s_read[1].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_Hmi_TotalCuttingQty.Invoke(dui, new object[] { "横切刀限制次数设定:" + "\t" + rs_data.Attributes["Hmi_TotalCuttingQty"].Value, 2 });
                }

            }
            if (s_read[2].IsConnected)
            {
                if (s_read[2].SyncRead(500))
                {
                    //送料速度
                    rs_data.Attributes["Hmi_MovingSpeed"].Value = s_read[2].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_Hmi_MovingSpeed.Invoke(dui, new object[] { "送料速度:" + "\t" + rs_data.Attributes["Hmi_MovingSpeed"].Value + "m/min", 3 });
                }

            }
            if (s_read[3].IsConnected)
            {
                if (s_read[3].SyncRead(500))
                {
                    //当前裁切次数
                    rs_data.Attributes["NowcuttingQty"].Value = s_read[3].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_NowcuttingQty.Invoke(dui, new object[] { "当前裁切次数:" + "\t" + rs_data.Attributes["NowcuttingQty"].Value + "次", 4 });
                }

            }
            if (s_read[4].IsConnected)
            {
                if (s_read[4].SyncRead(500))
                {
                    //当前总裁切次数
                    rs_data.Attributes["TotalCuttingQty"].Value = s_read[4].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_TotalCuttingQty.Invoke(dui, new object[] { "当前总裁切次数:" + "\t" + rs_data.Attributes["TotalCuttingQty"].Value + "次", 5 });
                }

            }
            //Debug.WriteLine("s_read[5].IsConnected", s_read[5].IsConnected);
            if (s_read[5].IsConnected)
            {
                if (s_read[5].SyncRead(500))
                {
                    //当前总送料米数
                    rs_data.Attributes["Length_MainMovingaxis_Total"].Value = ((double)s_read[5].Data.Value).ToString("f2");
                    DisplayUI dui = new DisplayUI(showText);
                    lab_gbl_length_MainMovingaxis_total.Invoke(dui, new object[] { "当前总送料米数:" + "\t" + rs_data.Attributes["Length_MainMovingaxis_Total"].Value + "m", 6 });
                }

            }
            if (s_read[6].IsConnected)
            {
                if (s_read[6].SyncRead(500))
                {
                    //PP料号
                    rs_data.Attributes["Hmi_PPtype"].Value = s_read[6].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_PPtype.Invoke(dui, new object[] { "PP料号:" + "\t" + rs_data.Attributes["Hmi_PPtype"].Value, 7 });
                    //Debug.WriteLine("PP料号:{0}", rs_data.Attributes["Hmi_PPtype"].Value);

                }

            }
            if (s_read[7].IsConnected)
            {
                if (s_read[7].SyncRead(500))
                {
                    //裁切次数
                    rs_data.Attributes["Hmi_SetCuttingQty"].Value = s_read[7].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_hmi_setcuttingqty.Invoke(dui, new object[] { "裁切次数:" + "\t" + rs_data.Attributes["Hmi_SetCuttingQty"].Value + "次", 8 });
                    //Debug.WriteLine("裁切次数:{0}", rs_data.Attributes["Hmi_SetCuttingQty"].Value);
                }

            }
            if (s_read[8].IsConnected)
            {
                if (s_read[8].SyncRead(500))
                {
                    //裁切长度
                    rs_data.Attributes["Hmi_CuttingLength"].Value = s_read[8].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    lab_Hmi_CuttingLength.Invoke(dui, new object[] { "裁切长度:" + "\t" + rs_data.Attributes["Hmi_CuttingLength"].Value + "mm", 9 });
                }

            }

            if (s_read[9].IsConnected)
            {
                if (s_read[9].SyncRead(500))
                {
                    //机器运行状态
                    rs_data.Attributes["Qbo_Lamp"].Value = s_read[9].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    string status = bool.Parse(rs_data.Attributes["Qbo_Lamp"].Value.ToString()) == true ? "Run" : "Stop";
                    lab_Qbo_Lamp.Invoke(dui, new object[] { "机器运行状态:" + "\t" + status, 10 });
                }

            }
        }

        private void showText(string text, int choose)
        {
            switch (choose)
            {
                case 1:
                    //纵切刀限制米数设定
                    if (lab_Hmi_Total_CrossCuttingQty.IsHandleCreated)
                    {
                        lab_Hmi_Total_CrossCuttingQty.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_Hmi_Total_CrossCuttingQty.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 2:
                    //横切刀限制次数设定
                    if (lab_Hmi_TotalCuttingQty.IsHandleCreated)
                    {
                        lab_Hmi_TotalCuttingQty.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_Hmi_TotalCuttingQty.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 3:
                    //送料速度
                    if (lab_Hmi_MovingSpeed.IsHandleCreated)
                    {
                        lab_Hmi_MovingSpeed.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_Hmi_MovingSpeed.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 4:
                    //当前裁切次数
                    if (lab_NowcuttingQty.IsHandleCreated)
                    {
                        lab_NowcuttingQty.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_NowcuttingQty.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 5:
                    //当前总裁切次数
                    if (lab_TotalCuttingQty.IsHandleCreated)
                    {
                        lab_TotalCuttingQty.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_TotalCuttingQty.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 6:
                    //当前总送料米数
                    if (lab_gbl_length_MainMovingaxis_total.IsHandleCreated)
                    {
                        lab_gbl_length_MainMovingaxis_total.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_gbl_length_MainMovingaxis_total.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 7:
                    //PP料号
                    if (lab_PPtype.IsHandleCreated)
                    {
                        lab_PPtype.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_PPtype.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 8:
                    //裁切次数
                    if (lab_hmi_setcuttingqty.IsHandleCreated)
                    {
                        lab_hmi_setcuttingqty.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_hmi_setcuttingqty.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 9:
                    //裁切长度
                    if (lab_hmi_setcuttingqty.IsHandleCreated)
                    {
                        lab_Hmi_CuttingLength.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_Hmi_CuttingLength.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                case 10:
                    //机器运行状态
                    if (lab_Qbo_Lamp.IsHandleCreated)
                    {
                        lab_Qbo_Lamp.Text = text;
                    }
                    else
                    {
                        DisplayUI dui = new DisplayUI(showText);
                        lab_Qbo_Lamp.Invoke(dui, new object[] { text, choose });
                    }
                    break;
                default:
                    break;
            }

        }

        void timer_Tick(object sender, EventArgs e)
        {
            led_status_server.BlinkMode = LedBlinkMode.BlinkWhenOn;
            // led_status_opc.BlinkMode = LedBlinkMode.BlinkWhenOn;

            lab_sysTime.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            foreach (var item in tb_layalarm.Controls)
            {
                if (item.GetType() == typeof(AlarmControl))
                {
                    AlarmControl ac = item as AlarmControl;
                    ac.led1.BlinkMode = LedBlinkMode.BlinkWhenOn;
                }
            }
        }


        private void btn_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmMain_Paint(object sender, PaintEventArgs e)
        {
            //SetPanelWidth();

        }

        private void SetPanelWidth()
        {
            //设置右侧panel宽度 
            panel_right.Width = 210;
            Rectangle rect = Screen.GetWorkingArea(this);
            panel_right.Width = (rect.Width - panel_left.Width);

        }


        private void FrmMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.Escape == e.KeyCode)
            {
                this.Close();
            }
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            if (TaskSatus.isGet != true && TaskSatus.isStart != true)
            {
                MessageBox.Show("工单信息未就绪，无法进入加工状态");
                return;
            }
            plc_get_timer = new System.Timers.Timer();
            plc_get_timer.Interval = 1000; //每隔1秒发送一次
            plc_get_timer.Elapsed += new System.Timers.ElapsedEventHandler(tc_timer_Elapsed);
            plc_get_timer.AutoReset = true;
            plc_get_timer.Start();
            led_work_status.Value = true;


        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            //if (s1.IsConnected)
            //{
            //    s1.SyncDisconnect(new TimeSpan(0, 0, 0, 0, 500));
            //}
            if (plc_get_timer != null)
            {
                plc_get_timer.Stop();
                plc_get_timer.Dispose();
            }

            led_work_status.Value = false;
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dr = MessageBox.Show("是否退出?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (DialogResult.Yes == dr)
            {
                //s1.SyncDisconnect(new TimeSpan(0, 0, 0, 0, 500));
                if (plc_get_timer != null)
                {
                    plc_get_timer.Stop();
                    plc_get_timer.Dispose();
                }
                if (frm_timer != null)
                {
                    frm_timer.Stop();
                    frm_timer.Dispose();
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void configDevice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            WCSApi.Get += new WcsApi.WCSApi.GetMessage(MessageRecieve);
            WCSApi.ShowNotice += new WcsApi.WCSApi.GetMessage(NoticeRecieve);
            FrmConfig config = new FrmConfig();
            config.ShowDialog();
            if (config.isOK)
            {
                lab_txtdevice.Text = PPDevice.DevicNO ?? "";
                ListViewItem lvitem = new ListViewItem();
                lvitem.ImageIndex = 7;
                if (PPDevice.IsConnectServer)
                {
                    led_status_server.Value = true;

                    lvitem.SubItems[0].Text = "服务器已连接";
                    lv_workinfo.Items.Add(lvitem);
                    GetWorkOrder();
                }
                else
                {
                    lvitem.SubItems[0].Text = "服务器连接失败";
                    lv_workinfo.Items.Add(lvitem);
                    led_status_server.Value = false;

                }
            }
        }

        private void btn_set_params_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(wk_order.SubTaskID))
            {
                MessageBox.Show("请先获派工单信息");
                return;
            }
            else
            {
                if (s_read[10].IsConnected)
                {
                    s_read[10].Data.Value = wk_order.MaterNo;
                    s_read[10].Update();
                    if (s_read[10].IsDataUpdated)
                    {
                        //PP料号
                        rs_data.Attributes["Hmi_PPtype"].Value = wk_order.MaterNo.ToString();

                        //Send_OK
                        DataSocket ds = new DataSocket();
                        XElement target = XmlHelper.QueryElements(element, "target", "name", "gbo_send_ok").FirstOrDefault();
                        sockect_url.Url_Send_OK = XmlHelper.QueryElement(target, "url").Value;
                        ds.Connect(sockect_url.Url_Send_OK, AccessMode.WriteAutoUpdate);
                        if (ds.IsConnected)
                        {
                            ds.Data.Value = 1;
                            ds.Update();
                            if (ds.IsDataUpdated)
                            {
                                rs_data.Attributes["Send_OK"].Value = true;
                                MessageBox.Show("下发参数成功！");
                            }
                            else
                            {
                                MessageBox.Show("下发参数失败！");
                            }
                        }
                    }

                }
            }

        }

        private void contextMenuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            if (toolStripMenuItem1.Selected)
            {
                lv_workinfo.Items.Clear();
            }

        }

        private void btn_wkorder_getParams_Click(object sender, EventArgs e)
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {
                var model = new IMesbus();
                model.Code = "CPS1003";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("LineCode", wk_order.LineCode);
                newdic.Add("SubTaskID", wk_order.SubTaskID);

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
            }
        }

        private void btn_wkorder_getted_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(wk_order.SubTaskID))
            {
                MessageBox.Show("请先获派工单信息");
                return;
            }
            else
            {
                IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
                string ipAddress = myEntry.AddressList[2].ToString();
                if (PPDevice.IsConnectServer)
                {
                    var model = new IMesbus();
                    model.Code = "CPS1002";
                    model.Company = PPDevice.Company;
                    model.Id = r.Next(1, 100);
                    model.Device = PPDevice.DevicNO;
                    model.Check = ipAddress;
                    var param = new IMebusParam();
                    param.Param = new List<Dictionary<string, string>>();
                    var newdic = new Dictionary<string, string>();
                    newdic.Add("SubTaskID", wk_order.SubTaskID);
                    param.Param.Add(newdic);
                    model.Data = param;
                    WCSApi.Send(_serial.Serialize(model));
                }

            }
        }

        private void btn_wkorder_start_Click(object sender, EventArgs e)
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {
                var model = new IMesbus();
                model.Code = "CPS1004";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("SubTaskID", wk_order.SubTaskID);

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
            }
        }

        private void btn_wkorder_finished_Click(object sender, EventArgs e)
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {
                var model = new IMesbus();
                model.Code = "CPS1005";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("SubTaskID", wk_order.SubTaskID);
                newdic.Add("CompleteQuantity", wk_order.PlanQuantity.ToString());

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
            }

        }

        private void btn_wkorder_newget_Click(object sender, EventArgs e)
        {
            if (!TaskSatus.isFinish)
            {
                DialogResult dr = MessageBox.Show("加工未完成，是否重新获取工单信息?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (DialogResult.OK == dr)
                {

                }
            }
            else
            {
                TaskSatus.isGet = false;
                TaskSatus.isStart = false;
                TaskSatus.isFinish = false;
                GetWorkOrder();

            }
        }

        private void btn_GetMater_Click(object sender, EventArgs e)
        {

        }

    }

}
