﻿namespace PPMachine
{
    partial class FrmWorkInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.led_status = new NationalInstruments.UI.WindowsForms.Led();
            this.btn_connect = new System.Windows.Forms.Button();
            this.btn_test = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_backUp = new System.Windows.Forms.Button();
            this.cmb_cmds = new System.Windows.Forms.ComboBox();
            this.txt_result = new System.Windows.Forms.TextBox();
            this.lab_TaskID = new System.Windows.Forms.Label();
            this.lab_PlanQuantity = new System.Windows.Forms.Label();
            this.lab_MaterNo = new System.Windows.Forms.Label();
            this.btn_callAGV = new System.Windows.Forms.Button();
            this.btn_query_device = new System.Windows.Forms.Button();
            this.gb_SubtaskInfo = new System.Windows.Forms.GroupBox();
            this.lab_SubTaskID = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.led_status)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.gb_SubtaskInfo.SuspendLayout();
            this.SuspendLayout();
            // 
            // led_status
            // 
            this.led_status.LedStyle = NationalInstruments.UI.LedStyle.Square3D;
            this.led_status.Location = new System.Drawing.Point(166, 24);
            this.led_status.Name = "led_status";
            this.led_status.Size = new System.Drawing.Size(72, 33);
            this.led_status.TabIndex = 0;
            // 
            // btn_connect
            // 
            this.btn_connect.Location = new System.Drawing.Point(30, 24);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(75, 33);
            this.btn_connect.TabIndex = 1;
            this.btn_connect.Text = "打开连接";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // btn_test
            // 
            this.btn_test.Location = new System.Drawing.Point(20, 104);
            this.btn_test.Name = "btn_test";
            this.btn_test.Size = new System.Drawing.Size(75, 23);
            this.btn_test.TabIndex = 2;
            this.btn_test.Text = "测试";
            this.btn_test.UseVisualStyleBackColor = true;
            this.btn_test.Click += new System.EventHandler(this.btn_test_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_backUp);
            this.groupBox1.Controls.Add(this.cmb_cmds);
            this.groupBox1.Controls.Add(this.txt_result);
            this.groupBox1.Controls.Add(this.btn_test);
            this.groupBox1.Location = new System.Drawing.Point(30, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 211);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test";
            // 
            // btn_backUp
            // 
            this.btn_backUp.Location = new System.Drawing.Point(20, 147);
            this.btn_backUp.Name = "btn_backUp";
            this.btn_backUp.Size = new System.Drawing.Size(75, 23);
            this.btn_backUp.TabIndex = 6;
            this.btn_backUp.Text = "物料还原";
            this.btn_backUp.UseVisualStyleBackColor = true;
            this.btn_backUp.Click += new System.EventHandler(this.btn_backUp_Click);
            // 
            // cmb_cmds
            // 
            this.cmb_cmds.FormattingEnabled = true;
            this.cmb_cmds.Items.AddRange(new object[] {
            "CPSTEST",
            "CPS1001",
            "CPS1002",
            "CPS1003",
            "CPS1004",
            "CPS1005",
            "CPS2001"});
            this.cmb_cmds.Location = new System.Drawing.Point(20, 37);
            this.cmb_cmds.Name = "cmb_cmds";
            this.cmb_cmds.Size = new System.Drawing.Size(105, 20);
            this.cmb_cmds.TabIndex = 5;
            // 
            // txt_result
            // 
            this.txt_result.Location = new System.Drawing.Point(136, 37);
            this.txt_result.Multiline = true;
            this.txt_result.Name = "txt_result";
            this.txt_result.Size = new System.Drawing.Size(215, 157);
            this.txt_result.TabIndex = 4;
            // 
            // lab_TaskID
            // 
            this.lab_TaskID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lab_TaskID.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_TaskID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lab_TaskID.Location = new System.Drawing.Point(301, 24);
            this.lab_TaskID.Name = "lab_TaskID";
            this.lab_TaskID.Size = new System.Drawing.Size(165, 38);
            this.lab_TaskID.TabIndex = 4;
            this.lab_TaskID.Text = "工单号：";
            this.lab_TaskID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_PlanQuantity
            // 
            this.lab_PlanQuantity.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lab_PlanQuantity.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_PlanQuantity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lab_PlanQuantity.Location = new System.Drawing.Point(44, 101);
            this.lab_PlanQuantity.Name = "lab_PlanQuantity";
            this.lab_PlanQuantity.Size = new System.Drawing.Size(244, 38);
            this.lab_PlanQuantity.TabIndex = 4;
            this.lab_PlanQuantity.Text = "计划产量：";
            this.lab_PlanQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lab_MaterNo
            // 
            this.lab_MaterNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lab_MaterNo.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_MaterNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lab_MaterNo.Location = new System.Drawing.Point(44, 51);
            this.lab_MaterNo.Name = "lab_MaterNo";
            this.lab_MaterNo.Size = new System.Drawing.Size(201, 38);
            this.lab_MaterNo.TabIndex = 4;
            this.lab_MaterNo.Text = "料号：";
            this.lab_MaterNo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_callAGV
            // 
            this.btn_callAGV.Location = new System.Drawing.Point(196, 356);
            this.btn_callAGV.Name = "btn_callAGV";
            this.btn_callAGV.Size = new System.Drawing.Size(95, 23);
            this.btn_callAGV.TabIndex = 5;
            this.btn_callAGV.Text = "呼叫AGV小车";
            this.btn_callAGV.UseVisualStyleBackColor = true;
            this.btn_callAGV.Click += new System.EventHandler(this.btn_callAGV_Click);
            // 
            // btn_query_device
            // 
            this.btn_query_device.Location = new System.Drawing.Point(196, 474);
            this.btn_query_device.Name = "btn_query_device";
            this.btn_query_device.Size = new System.Drawing.Size(95, 23);
            this.btn_query_device.TabIndex = 6;
            this.btn_query_device.Text = "查询设备状态";
            this.btn_query_device.UseVisualStyleBackColor = true;
            this.btn_query_device.Click += new System.EventHandler(this.btn_query_device_Click);
            // 
            // gb_SubtaskInfo
            // 
            this.gb_SubtaskInfo.Controls.Add(this.lab_PlanQuantity);
            this.gb_SubtaskInfo.Controls.Add(this.lab_MaterNo);
            this.gb_SubtaskInfo.Location = new System.Drawing.Point(486, 75);
            this.gb_SubtaskInfo.Name = "gb_SubtaskInfo";
            this.gb_SubtaskInfo.Size = new System.Drawing.Size(469, 247);
            this.gb_SubtaskInfo.TabIndex = 7;
            this.gb_SubtaskInfo.TabStop = false;
            this.gb_SubtaskInfo.Text = "派工单信息:";
            // 
            // lab_SubTaskID
            // 
            this.lab_SubTaskID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lab_SubTaskID.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_SubTaskID.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lab_SubTaskID.Location = new System.Drawing.Point(483, 24);
            this.lab_SubTaskID.Name = "lab_SubTaskID";
            this.lab_SubTaskID.Size = new System.Drawing.Size(185, 38);
            this.lab_SubTaskID.TabIndex = 4;
            this.lab_SubTaskID.Text = "当前派工单：";
            this.lab_SubTaskID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(533, 386);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 8;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // FrmWorkInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 627);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gb_SubtaskInfo);
            this.Controls.Add(this.btn_query_device);
            this.Controls.Add(this.btn_callAGV);
            this.Controls.Add(this.lab_SubTaskID);
            this.Controls.Add(this.lab_TaskID);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_connect);
            this.Controls.Add(this.led_status);
            this.Name = "FrmWorkInfo";
            this.Text = "FrmWorkInfo";
            this.Load += new System.EventHandler(this.FrmWorkInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.led_status)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gb_SubtaskInfo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private NationalInstruments.UI.WindowsForms.Led led_status;
        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.Button btn_test;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txt_result;
        private System.Windows.Forms.ComboBox cmb_cmds;
        private System.Windows.Forms.Label lab_TaskID;
        private System.Windows.Forms.Label lab_PlanQuantity;
        private System.Windows.Forms.Button btn_backUp;
        private System.Windows.Forms.Label lab_MaterNo;
        private System.Windows.Forms.Button btn_callAGV;
        private System.Windows.Forms.Button btn_query_device;
        private System.Windows.Forms.GroupBox gb_SubtaskInfo;
        private System.Windows.Forms.Label lab_SubTaskID;
        private System.Windows.Forms.Button button1;
    }
}