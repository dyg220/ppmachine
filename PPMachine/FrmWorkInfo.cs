﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using WCS.API;
using WcsApi;
using System.Web.Script.Serialization;
using System.Diagnostics;

namespace PPMachine
{
    public partial class FrmWorkInfo : Form
    {
        public FrmWorkInfo()
        {
            InitializeComponent();
        }
        Random r = new Random();
        static JavaScriptSerializer _serial = new JavaScriptSerializer();
        WorkOrder wk_order = new WorkOrder();
        public static string LineCode = "ZHENGYE001";

        public int CompleteQuantity = 72; //完成数量

        public delegate void UpDateUI(WorkOrder model, string buscode);

        private void btn_connect_Click(object sender, EventArgs e)
        {
            WcsApi.WCSApi.Get += new WcsApi.WCSApi.GetMessage(MessageRecieve);
            WcsApi.WCSApi.ShowNotice += new WcsApi.WCSApi.GetMessage(NoticeRecieve);

            FrmConfig config = new FrmConfig();
            DialogResult dr = config.ShowDialog();
            if (config.isOK)
            {
                if (PPDevice.IsConnectServer)
                {
                    led_status.Value = true;
                }
            }

        }

        void NoticeRecieve(string message)
        {

        }
        public void showMsg(WorkOrder model, string buscode)
        {
            switch (buscode)
            {
                case "CPSTEST":
                    if (txt_result.IsHandleCreated)
                    {
                        txt_result.Text = model.welcome;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        txt_result.Invoke(update, new object[] { model, buscode });
                    }
                    break;
                case "CPS1001":
                    if (lab_TaskID.IsHandleCreated)
                    {
                        lab_TaskID.Text = "工单号：" + model.TaskID;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_TaskID.Invoke(update, new object[] { "工单号：" + model.SubTaskID, buscode });
                    }
                    if (lab_PlanQuantity.IsHandleCreated)
                    {
                        lab_PlanQuantity.Text = "计划产量：" + model.PlanQuantity;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_PlanQuantity.Invoke(update, new object[] { "计划产量：" + model.PlanQuantity, buscode });
                    }
                    if (lab_MaterNo.IsHandleCreated)
                    {
                        lab_MaterNo.Text = "PP料号：" + model.MaterNo;
                    }
                    else
                    {
                        UpDateUI update = new UpDateUI(showMsg);
                        lab_MaterNo.Invoke(update, new object[] { "PP料号：" + model.MaterNo, buscode });
                    }
                    break;

                default:
                    break;
            }
        }


        void MessageRecieve(string message)
        {
            IMesbus bus = (IMesbus)_serial.Deserialize(message, typeof(IMesbus));
            if (bus.Data.Code == "Success")
            {
                if (bus.Code.Equals("CPSTEST"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.welcome = obj["CPSWelcome"].ToString();
                    UpDateUI update = new UpDateUI(showMsg);
                    txt_result.Invoke(update, new object[] { wk_order, bus.Code });

                }
                if (bus.Code.Equals("CPS1001"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.CustomCode = obj["CustomCode"].ToString();
                    wk_order.SubTaskID = obj["SubTaskID"].ToString();
                    wk_order.TaskID = obj["TaskID"].ToString();
                    wk_order.PlanQuantity = int.Parse(obj["PlanQuantity"].ToString());//计划产量
                    wk_order.LineCode = obj["LineCode"].ToString();
                    wk_order.MaterLotNo = obj["MaterLotNo"].ToString();
                    wk_order.MaterNo = obj["MaterNo"].ToString();
                    wk_order.NextLineCode = obj["NextLineCode"].ToString();
                    wk_order.CurrProcCode = obj["CurrProcCode"].ToString();

                    UpDateUI update = new UpDateUI(showMsg);
                    lab_TaskID.Invoke(update, new object[] { wk_order, bus.Code });
                    lab_MaterNo.Invoke(update, new object[] { wk_order, bus.Code });
                }

                if (bus.Code.Equals("CPS1002"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.updateInfo = int.Parse(obj["ReturnCode"].ToString()) > 0 ? "更新成功" : "更新失败";
                    MessageBox.Show(wk_order.updateInfo);
                }
                if (bus.Code.Equals("CPS1003"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.DeviceCode = obj["DeviceCode"].ToString();
                    wk_order.MaterLotNo = obj["MaterLotNo"].ToString();
                    wk_order.LineCode = obj["LineCode"].ToString();
                    wk_order.CSPSCode = obj["CSPSCode"].ToString();


                }
                if (bus.Code.Equals("CPS1004"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        MessageBox.Show("更新成功");
                    }

                    else
                    {
                        MessageBox.Show("更新失败:" + obj["ReturnMsg"].ToString());
                    }
                }
                if (bus.Code.Equals("CPS1005"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        MessageBox.Show("更新成功");
                    }

                    else
                    {
                        MessageBox.Show("更新失败:" + obj["ReturnMsg"].ToString());
                    }
                }


                if (bus.Code.Equals("CPS000"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.updateInfo = obj["ReturnMsg"].ToString();
                    MessageBox.Show(wk_order.updateInfo);
                }

                if (bus.Code.Equals("CPS2001"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        MessageBox.Show("呼叫AGV小车成功");
                    }

                    else
                    {
                        MessageBox.Show("呼叫AGV小车失败:" + obj["ReturnMsg"].ToString());
                    }
                }

                if (bus.Code.Equals("CPS3001"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        MessageBox.Show("设备已就绪");
                    }

                    else
                    {
                        MessageBox.Show("设备未就绪:" + obj["ReturnMsg"].ToString());
                    }
                }

            }

            else if (bus.Data.Code == "Error")
            {
                MessageBox.Show(bus.Data.Returns.ToString());
            }

        }

        private void btn_test_Click(object sender, EventArgs e)
        {
            string cmd = cmb_cmds.SelectedItem.ToString();
            if (!string.IsNullOrEmpty(cmd))
            {
                if (PPDevice.IsConnectServer)
                {

                    #region 构造iMesbus协议
                    var model = new IMesbus();
                    model.Code = cmd;
                    model.Company = PPDevice.Company;
                    model.Id = r.Next(1, 100);
                    model.Device = PPDevice.DevicNO;
                    model.Check = PPDevice.DevicIP;
                    var param = new IMebusParam();
                    param.Param = new List<Dictionary<string, string>>();
                    var newdic = new Dictionary<string, string>();
                    switch (cmd)
                    {
                        case "CPS1001":
                            newdic.Add("LineCode", LineCode);
                            break;
                        case "CPS1002":
                            newdic.Add("SubTaskID", wk_order.SubTaskID.ToString());  //修改派工单状态为开始
                            break;
                        case "CPS1003":
                            newdic.Add("LineCode", LineCode);
                            newdic.Add("SubTaskID", wk_order.SubTaskID);

                            break;
                        case "CPS1004":
                            //newdic.Add("LineCode", LineCode);
                            newdic.Add("SubTaskID", wk_order.SubTaskID);
                            break;
                        case "CPS1005":
                            newdic.Add("SubTaskID", wk_order.SubTaskID);
                            newdic.Add("CompleteQuantity", CompleteQuantity.ToString());
                            break;

                        case "CPS2001":
                            newdic.Add("WorkCode", "zhengye001_A");
                            newdic.Add("LineCode", CompleteQuantity.ToString());
                            newdic.Add("TaskId", wk_order.TaskID);
                            newdic.Add("TaskStyle", Enum.Parse(typeof(TaskStyle), "ts0", true).ToString());
                            newdic.Add("DeviceCode", wk_order.DeviceCode);
                            //newdic.Add("TaskProcCode",wk_order.);
                            newdic.Add("MaterLotNo", wk_order.MaterLotNo);
                            newdic.Add("MaterQuantity", wk_order.MaterQuantity.ToString());
                            // newdic.Add("PlatCode", wk_order.);
                            newdic.Add("NextWorkCode", wk_order.NextLineCode);
                            newdic.Add("ClientType", "0");
                            newdic.Add("CurrProcCode", wk_order.CurrProcCode);
                            break;



                        default:
                            break;
                    }
                    param.Param.Add(newdic);
                    model.Data = param;
                    WCSApi.Send(_serial.Serialize(model));

                    #endregion
                }

            }

        }

        private void FrmWorkInfo_Load(object sender, EventArgs e)
        {
            cmb_cmds.SelectedIndex = 0;
        }

        private void btn_backUp_Click(object sender, EventArgs e)
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {

                var model = new IMesbus();
                model.Code = "CPS000";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();
                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));

            }
        }

        private void btn_callAGV_Click(object sender, EventArgs e)
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {

                var model = new IMesbus();
                model.Code = "CPS2001";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();
                Material mt = new Material();
                mt.WorkCode = "zhengye001_A"; // zhengye001_A
                mt.LineCode = wk_order.LineCode;
                mt.TaskId = wk_order.TaskID;
                mt.taskStyle = TaskStyle.ts0;
                mt.DeviceCode = wk_order.DeviceCode; //cps通讯就不用填
                mt.TaskProcCode = wk_order.CustomCode;
                mt.MaterLotNo = wk_order.MaterLotNo;
                mt.MaterQuantity = wk_order.MaterQuantity.ToString();
                mt.PlatCode = "";
                mt.NextWorkCode = wk_order.NextLineCode;
                mt.EarlyArriveTm = DateTime.Now.AddHours(1);
                mt.LastArriveTm = DateTime.Now.AddHours(2.0);
                mt.ClientType = "1";
                mt.CurrProcCode = wk_order.CurrProcCode;

                newdic.Add("TaskId", mt.TaskId);
                newdic.Add("TaskStyle", ((int)(TaskStyle)Enum.Parse(typeof(TaskStyle), "ts0")).ToString());
                newdic.Add("TaskProcCode", mt.TaskProcCode);
                newdic.Add("MaterLotNo", mt.MaterLotNo);
                newdic.Add("WorkCode", mt.WorkCode);
                newdic.Add("NextWorkCode", mt.NextWorkCode);
                newdic.Add("MaterQuantity", mt.MaterQuantity);
                newdic.Add("EarlyArriveTm", mt.EarlyArriveTm.ToString("yyyy-MM-dd hh:mm:ss"));
                newdic.Add("LastArriveTm", mt.LastArriveTm.ToString("yyyy-MM-dd hh:mm:ss"));
                newdic.Add("ClientType", mt.ClientType);
                newdic.Add("CurrProcCode", mt.CurrProcCode);
                newdic.Add("LineCode", mt.LineCode);
                newdic.Add("PlatCode", mt.PlatCode);
                newdic.Add("NextLineCode", wk_order.NextLineCode);
                newdic.Add("DeviceCode", mt.DeviceCode);

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));

            }


        }

        private void btn_query_device_Click(object sender, EventArgs e)
        {
            IPHostEntry myEntry = Dns.GetHostEntry(Dns.GetHostName());
            string ipAddress = myEntry.AddressList[2].ToString();
            if (PPDevice.IsConnectServer)
            {

                var model = new IMesbus();
                model.Code = "CPS3001";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = ipAddress;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("DeviceCode", wk_order.DeviceCode);
                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
          
        }
    }
}
