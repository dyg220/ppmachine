﻿namespace PPMachine.Common
{
    partial class AlarmControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.lab_title = new System.Windows.Forms.Label();
            this.led1 = new NationalInstruments.UI.WindowsForms.Led();
            ((System.ComponentModel.ISupportInitialize)(this.led1)).BeginInit();
            this.SuspendLayout();
            // 
            // lab_title
            // 
            this.lab_title.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lab_title.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lab_title.ForeColor = System.Drawing.Color.Teal;
            this.lab_title.Location = new System.Drawing.Point(25, 0);
            this.lab_title.Margin = new System.Windows.Forms.Padding(3);
            this.lab_title.Name = "lab_title";
            this.lab_title.Size = new System.Drawing.Size(69, 28);
            this.lab_title.TabIndex = 1;
            this.lab_title.Text = "报警信息";
            this.lab_title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // led1
            // 
            this.led1.LedStyle = NationalInstruments.UI.LedStyle.Square3D;
            this.led1.Location = new System.Drawing.Point(0, 1);
            this.led1.Name = "led1";
            this.led1.Size = new System.Drawing.Size(24, 23);
            this.led1.TabIndex = 2;
            // 
            // AlarmControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.led1);
            this.Controls.Add(this.lab_title);
            this.Name = "AlarmControl";
            this.Size = new System.Drawing.Size(94, 28);
            ((System.ComponentModel.ISupportInitialize)(this.led1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected internal System.Windows.Forms.Label lab_title;
        protected internal NationalInstruments.UI.WindowsForms.Led led1;

    }
}
