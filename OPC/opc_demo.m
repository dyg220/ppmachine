function varargout = opc_demo(varargin)
%OPC_DEMO M-file for opc_demo.fig
%      OPC_DEMO, by itself, creates a new OPC_DEMO or raises the existing
%      singleton*.
%
%      H = OPC_DEMO returns the handle to a new OPC_DEMO or the handle to
%      the existing singleton*.
%
%      OPC_DEMO('Property','Value',...) creates a new OPC_DEMO using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to opc_demo_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      OPC_DEMO('CALLBACK') and OPC_DEMO('CALLBACK',hObject,...) call the
%      local function named CALLBACK in OPC_DEMO.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help opc_demo

% Last Modified by GUIDE v2.5 28-May-2010 14:30:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @opc_demo_OpeningFcn, ...
                   'gui_OutputFcn',  @opc_demo_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before opc_demo is made visible.
function opc_demo_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for opc_demo

handles.guifig = gcf;
warning off;

global SINE1 PRESSURE ACCELERATION TEMPERATURE RPM HUMIDITY CYLINDER EJECTOR

SINE1 = 'Channel_0.Sine.Sine1';
PRESSURE = 'Channel_0.Random.Pressure';
ACCELERATION = 'Channel_1.Device_1.Acceleration';
TEMPERATURE = 'Channel_2.Device_3.Temperature';
RPM = 'Channel_2.Device_3.RPM';
HUMIDITY = 'Channel_2.Device_3.Humidity';
CYLINDER = 'Channel_1.Device_2.Cylinder';
EJECTOR = 'Channel_4.Device_5.Ejector';

axes(handles.axes1)
handles.x1 = NaN(1, 10000);
handles.y1 = NaN(1, 10000);
handles.sine1 = plot(handles.x1,handles.y1);
axis on;

handles.counter = 1;
%OPC Initialisation
try
    handles.da = opcda('localhost','KEPware.KEPServerEx.V4');%defines the Local Host as KepServer
    connect(handles.da);%connects to Kepserver
catch
    opcregister('install', '-silent');%if not able to connect then silently install the opc
    connect(handles.da);%connects to Kepserver
end

try
    handles.data = addgroup(handles.da, 'DataGroup'); %make a DataGroup "Group" for checking the status of the Data of PLC
    handles.dataIDS = {SINE1 PRESSURE ACCELERATION TEMPERATURE RPM HUMIDITY CYLINDER EJECTOR}; %list out all dataIDS from the global variables shown above
    handles.data_itm = additem(handles.data, handles.dataIDS); %an itm object is created with all above listed items tags
    guidata(handles.guifig, handles);

    set(handles.data,'DataChangeFcn',{@Data_Group_Callback,handles},'UpdateRate',0);%CALLBACK FUNCTION for any change in the DATA of OPC structure
end

try
    state = find(strcmp(handles.dataIDS,CYLINDER));%READS VALUE OF CYLINDER FROM PLC
    state = read(handles.data_itm(state));
    state = double(state.Value);%state of the CYLINDER
    if state == 0
        set(handles.cylinder,'Value',0);
        set(handles.cylinder,'BackGroundColor',[1 0 0]);
    else
        set(handles.cylinder,'Value',1);
        set(handles.cylinder,'BackGroundColor',[0 1 0]);
    end

    state = find(strcmp(handles.dataIDS,EJECTOR));%READS VALUE OF EJECTOR FROM PLC
    state = read(handles.data_itm(state));
    state = double(state.Value);%state of the EJECTOR
    if state == 0
        set(handles.ejector,'Value',0);
        set(handles.ejector,'BackGroundColor',[1 0 0]);
    else
        set(handles.ejector,'Value',1);
        set(handles.ejector,'BackGroundColor',[0 1 0]);
    end
end
handles.output = hObject;


% Update handles structure
guidata(hObject, handles);
guidata(handles.guifig, handles);
% UIWAIT makes opc_demo wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = opc_demo_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --------------------------------------------------------------------
%Executes on getting an event from PLC
function Data_Group_Callback(hObject, eventdata, handles)
%This will be called only when there is a physical change in the state of data of PLC
global SINE1 PRESSURE ACCELERATION TEMPERATURE RPM HUMIDITY CYLINDER EJECTOR

handles = guidata(handles.guifig);

ev = read(handles.data);%read all items and its value in ev
ev_type = {ev.ItemID};%all item names in ev_type
ev_val = [ev.Value];%all items value in ev_val

try
    
    if ev_val(find(strcmp(ev_type,PRESSURE))) %if PRESSURE is got from PLC
        val = ev_val(find(strcmp(ev_type,PRESSURE)));
        set(handles.pressure,'String',num2str(val)); %update PRESSURE
        drawnow;
    end

    if ev_val(find(strcmp(ev_type,ACCELERATION))) %if ACCELERATION is got from PLC
        val = ev_val(find(strcmp(ev_type,ACCELERATION)));
        set(handles.acceleration,'String',num2str(val)); %update ACCELERATION
        drawnow;
    end

    if ev_val(find(strcmp(ev_type,TEMPERATURE))) %if TEMPERATURE is got from PLC
        val = ev_val(find(strcmp(ev_type,TEMPERATURE)));
        set(handles.temperature,'String',num2str(val)); %update TEMPERATURE
        drawnow;
    end

    if ev_val(find(strcmp(ev_type,RPM))) %if RPM is got from PLC
        val = ev_val(find(strcmp(ev_type,RPM)));
        set(handles.rpm,'String',num2str(val)); %update RPM
        drawnow;
    end

    if ev_val(find(strcmp(ev_type,HUMIDITY))) %if HUMIDITY is got from PLC
        val = ev_val(find(strcmp(ev_type,HUMIDITY)));
        set(handles.humidity,'String',num2str(val)); %update HUMIDITY
        drawnow;
    end

%     if ev_val(find(strcmp(ev_type,SINE1))) %if SINE1 is got from PLC
        state = find(strcmp(handles.dataIDS,SINE1));%READS VALUE OF SINE1 FROM PLC
        state = read(handles.data_itm(state));
        state = double(state.Value);%state of the SINE1
        handles.x1(handles.counter) = handles.counter;
        handles.y1(handles.counter) = state;
        set(handles.sine1,'XData',handles.x1,'YData',handles.y1);
        drawnow;
%     end
    handles.counter = handles.counter + 1;

    guidata(handles.guifig, handles);
end




function pressure_Callback(hObject, eventdata, handles)
% hObject    handle to pressure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pressure as text
%        str2double(get(hObject,'String')) returns contents of pressure as a double


% --- Executes during object creation, after setting all properties.
function pressure_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pressure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function acceleration_Callback(hObject, eventdata, handles)
% hObject    handle to acceleration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of acceleration as text
%        str2double(get(hObject,'String')) returns contents of acceleration as a double


% --- Executes during object creation, after setting all properties.
function acceleration_CreateFcn(hObject, eventdata, handles)
% hObject    handle to acceleration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function temperature_Callback(hObject, eventdata, handles)
% hObject    handle to temperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of temperature as text
%        str2double(get(hObject,'String')) returns contents of temperature as a double


% --- Executes during object creation, after setting all properties.
function temperature_CreateFcn(hObject, eventdata, handles)
% hObject    handle to temperature (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function rpm_Callback(hObject, eventdata, handles)
% hObject    handle to rpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of rpm as text
%        str2double(get(hObject,'String')) returns contents of rpm as a double


% --- Executes during object creation, after setting all properties.
function rpm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to rpm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function humidity_Callback(hObject, eventdata, handles)
% hObject    handle to humidity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of humidity as text
%        str2double(get(hObject,'String')) returns contents of humidity as a double


% --- Executes during object creation, after setting all properties.
function humidity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to humidity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in cylinder.
function cylinder_Callback(hObject, eventdata, handles)
% hObject    handle to cylinder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of cylinder
global SINE1 PRESSURE ACCELERATION TEMPERATURE RPM HUMIDITY CYLINDER EJECTOR

state = find(strcmp(handles.dataIDS,CYLINDER));%READS VALUE OF CYLINDER FROM PLC
state = read(handles.data_itm(state));
state = double(state.Value);%state of the CYLINDER
if state == 0
    val = find(strcmp(handles.dataIDS,CYLINDER));
    write(handles.data_itm(val),1);%writes value to CYLINDER
    set(handles.cylinder,'Value',1);
    set(handles.cylinder,'BackGroundColor',[0 1 0]);    
else
    val = find(strcmp(handles.dataIDS,CYLINDER));
    write(handles.data_itm(val),0);%writes value to CYLINDER
    set(handles.cylinder,'Value',0);
    set(handles.cylinder,'BackGroundColor',[1 0 0]);
end

guidata(handles.guifig, handles);
% --- Executes on button press in ejector.
function ejector_Callback(hObject, eventdata, handles)
% hObject    handle to ejector (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ejector
global SINE1 PRESSURE ACCELERATION TEMPERATURE RPM HUMIDITY CYLINDER EJECTOR

state = find(strcmp(handles.dataIDS,EJECTOR));%READS VALUE OF EJECTOR FROM PLC
state = read(handles.data_itm(state));
state = double(state.Value);%state of the EJECTOR
if state == 0
    val = find(strcmp(handles.dataIDS,EJECTOR));
    write(handles.data_itm(val),1);%writes value to EJECTOR
    set(handles.ejector,'Value',1);
    set(handles.ejector,'BackGroundColor',[0 1 0]);
else
    val = find(strcmp(handles.dataIDS,EJECTOR));
    write(handles.data_itm(val),0);%writes value to EJECTOR
    set(handles.ejector,'Value',0);
    set(handles.ejector,'BackGroundColor',[1 0 0]);
end

guidata(handles.guifig, handles);
% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: delete(hObject) closes the figure

% try
%     set(handles.data,'Active','off');
%     set(handles.data,'Subscription','off');
% end
pause(0.5);
try
    delete(handles.data);
end

try
    disconnect(handles.da);%Stops OPC server and removes from memory
end

delete(hObject);


