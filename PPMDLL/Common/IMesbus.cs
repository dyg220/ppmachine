﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPMDLL
{
    public class IMesbus
    {
        /// <summary>
        /// 随机事务ID,必填
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 机构号，填入999
        /// </summary>
        public string Company { get; set; }
        /// <summary>
        /// 设备号，必填
        /// </summary>
        public string Device { get; set; }
        /// <summary>
        /// 指令码，必填
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 数据包，必填19
        /// </summary>
        public IMebusParam Data { get; set; }
        /// <summary>
        /// 校验IP，必填
        /// </summary>
        public string Check { get; set; }
    }
    public class IMebusParam
    {
        /// <summary>
        /// 反馈码,发起时为空
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 反馈结果,发起时为空
        /// </summary>
        public bool Result { get; set; }
        /// <summary>
        /// 转发设备号，可为空
        /// </summary>
        public string DeviceCode { get; set; }
        /// <summary>
        /// 大数据参数
        /// </summary>
        public List<Dictionary<string, string>> Param { get; set; }
        /// <summary>
        /// 返回结果集,发起时为空
        /// </summary>
        public Object Returns { get; set; }
    }
}
