﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPMDLL
{
    /// <summary>
    /// 系统字体
    /// </summary>
    public class ProFonts
    {

        public static string SongTi = "宋体";

        public static string Arial = "Arial";

        public static string PixelLCD7 = "Pixel LCD7";

        public static string LcdD = "LcdD";

        public static string Yahei = "微软雅黑";


    }
}
