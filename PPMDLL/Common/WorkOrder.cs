﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPMDLL
{
    /// <summary>
    /// 工单信息
    /// 任务和物料信息
    /// </summary>
    public class WorkOrder
    {

        // 一个派工单对应的工单号是唯一的
        public string SubTaskID { set; get; } //派工单 ID 
        // 一个工单号有多个派工单
        public string TaskID { set; get; } //工单编号 
        //唯一工单号
        public string CVouchCode { set; get; }
        public int PlanQuantity { set; get; } // 计划产量
        public float PlanTime { set; get; }//计划需求总工时
        public string LineCode { set; get; }//产线编号（含 EMCS 和单机单元）
        public int ProcID { set; get; } //当前工序ID
        public string CurrProcCode { set; get; } //当前工序编号
        public string NextProcCode { set; get; } //下一工序编号
        public string NextLineCode { set; get; } //下一步产线设备编号（含 EMCS 和单机单元）
        public float StdManHour { set; get; }//标准工时
        public string CustomCode { set; get; } //客户料号，用来获取配方(产品编码+工序号)
        public string MaterLotNo { set; get; } //物料批次号
        public string MaterNo { set; get; } //料号
        public string NextMaterLotNo { set; get; } //下一物料批次号
        public int MaterQuantity { set; get; } //物料数量
        public string StartDate { set; get; } //计划开始时间
        public string EndDate { set; get; } //计划结束时间

        public string welcome { set; get; }



        public string CSPSCode { set; get; }
        public bool updatestatus { set; get; }
        public string updateInfo { set; get; }

    }

}
