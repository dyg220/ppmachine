﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPMDLL
{
    /// <summary>
    /// 处理指令返回消息
    /// </summary>
    public class RetMessage
    {
        public int ReturnCode { get; set; }

        public string ReturnMsg { get; set; }

    }
}
