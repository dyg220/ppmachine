﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using System.Threading;

namespace PPMDLL
{
    public class PPMTools
    {
        public static string LineCode { set; get; }   //设备号/产线号
        public static string DeviceCode { set; get; } //默认IMS
        public static string WorkCode { set; get; }  //工位号
        public static int TaskStyle { get; set; }   //任务类型：0:下料 1:上料 2:返修 3:下空架-上料 4:下料-上空架 5:上空料架 6:入库 7:出库
        public static int CompleteQuantity { set; get; } //完成数量
        public static string SubTaskID { set; get; } //派工单


        static Random r = new Random();
        static JavaScriptSerializer _serial = new JavaScriptSerializer();
        public static WorkOrder wk_order = new WorkOrder();

        static PPMTools()
        {

        }
        ~PPMTools()
        {
            wk_order = null;
            GC.Collect();
        }
        public static string Sendcmds(string cmd)
        {
            if (!string.IsNullOrEmpty(cmd))
            {
                #region 构造iMesbus协议
                var model = new IMesbus();
                model.Code = cmd;
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = PPDevice.DevicIP;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();
                switch (cmd)
                {
                    case "CPS1001":
                        newdic.Add("LineCode", LineCode);
                        break;
                    case "CPS1002":
                        newdic.Add("SubTaskID", wk_order.SubTaskID.ToString());  //修改派工单状态为开始
                        break;
                    case "CPS1003":         //请求参数
                        newdic.Add("LineCode", LineCode);
                        newdic.Add("SubTaskID", wk_order.SubTaskID);
                        break;
                    case "CPS1004":
                        newdic.Add("SubTaskID", wk_order.SubTaskID); //更新任务状态为开始
                        break;
                    case "CPS1005":
                        newdic.Add("SubTaskID", wk_order.SubTaskID);
                        newdic.Add("CompleteQuantity", CompleteQuantity.ToString());
                        break;
                    case "CPS2001":
                        newdic.Add("TaskId", wk_order.TaskID);
                        newdic.Add("TaskStyle", TaskStyle.ToString());
                        newdic.Add("TaskProcCode", wk_order.CustomCode);
                        newdic.Add("MaterLotNo", wk_order.MaterLotNo);
                        newdic.Add("WorkCode", WorkCode);
                        newdic.Add("NextWorkCode", wk_order.NextLineCode);
                        newdic.Add("MaterQuantity", wk_order.MaterQuantity.ToString());
                        newdic.Add("EarlyArriveTm", DateTime.Now.AddHours(1.0).ToString());
                        newdic.Add("LastArriveTm", DateTime.Now.AddHours(3.5).ToString());
                        newdic.Add("ClientType", "0");  //终端类型：产线单元
                        newdic.Add("CurrProcCode", wk_order.CurrProcCode);
                        newdic.Add("LineCode", LineCode);
                        newdic.Add("PlatCode", string.Empty);
                        newdic.Add("NextLineCode", string.Empty);
                        newdic.Add("DeviceCode", DeviceCode);
                        newdic.Add("CVouchCode", wk_order.CVouchCode);
                        break;

                    default:
                        break;
                }
                param.Param.Add(newdic);
                model.Data = param;
                //WCSApi.Send(_serial.Serialize(model));
                return _serial.Serialize(model);
                #endregion
            }
            return string.Empty;
        }

        static void NoticeRecieve(string message)
        {

        }
        public static string MessageRecieve(string message)
        {
            if (message.Contains("}{"))
            {
                var index = message.IndexOf("}{");
                message = message.Insert(index + 1, ",");
                message = message.Substring(index + 2);
                IMesbus bus = (IMesbus)_serial.Deserialize(message, typeof(IMesbus));
                if (bus.Data.Code == "Success")
                {
                    if (bus.Code.Equals("CPSTEST"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);
                        wk_order.welcome = obj["CPSWelcome"].ToString();
                        wk_order.StartDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                        wk_order.EndDate = DateTime.Now.AddDays(2.0).ToString("yyyy-MM-dd HH:mm:ss");

                    }
                    if (bus.Code.Equals("CPS1001"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);

                        wk_order.CustomCode = obj["CustomCode"].ToString();
                        wk_order.SubTaskID = obj["SubTaskID"].ToString();
                        wk_order.TaskID = obj["TaskID"].ToString();
                        wk_order.PlanQuantity = int.Parse(obj["PlanQuantity"].ToString());//计划产量
                        LineCode = obj["LineCode"].ToString();
                        wk_order.MaterLotNo = obj["MaterLotNo"].ToString();
                        wk_order.MaterNo = obj["MaterNo"].ToString();
                        wk_order.NextLineCode = obj["NextLineCode"].ToString();
                        wk_order.CurrProcCode = obj["CurrProcCode"].ToString();

                    }

                    if (bus.Code.Equals("CPS1002"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);
                        if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                        {
                            wk_order.updatestatus = true;
                            wk_order.updateInfo = "工单已领取";
                        }
                        else
                        {
                            wk_order.updatestatus = false;
                            wk_order.updateInfo = "工单领取失败";
                        }
                    }
                    if (bus.Code.Equals("CPS1003"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);

                        DeviceCode = obj["DeviceCode"].ToString();
                        LineCode = obj["LineCode"].ToString();
                        wk_order.MaterLotNo = obj["MaterLotNo"].ToString();
                        wk_order.CSPSCode = obj["CSPSCode"].ToString();

                        //先输出信息
                        wk_order.updateInfo = _serial.Serialize(result);
                        //File.WriteAllText("c:\\a.txt", wk_order.updateInfo);

                    }
                    if (bus.Code.Equals("CPS1004"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);

                        if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                        {
                            wk_order.updatestatus = true;
                            wk_order.updateInfo = "任务进行中...";
                        }
                        else
                        {
                            wk_order.updatestatus = false;
                            wk_order.updateInfo = "任务开始失败:" + obj["ReturnMsg"].ToString();
                        }
                    }
                    if (bus.Code.Equals("CPS1005"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);
                        if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                        {
                            wk_order.updatestatus = true;
                            wk_order.updateInfo = "任务发送已完成";
                        }
                        else
                        {
                            wk_order.updatestatus = false;
                            wk_order.updateInfo = "任务发送完成失败:" + obj["ReturnMsg"].ToString();
                        }
                    }
                    if (bus.Code.Equals("CPS000"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        var obj = (Dictionary<string, object>)(result[0]);

                        wk_order.updateInfo = obj["ReturnMsg"].ToString();
                    }
                    if (bus.Code.Equals("CPS2001"))
                    {
                        object[] result = (object[])bus.Data.Returns;

                        //先输出信息
                        wk_order.updateInfo = _serial.Serialize(result);
                        File.WriteAllText("c:\\a.txt", wk_order.updateInfo);
                        var obj = (Dictionary<string, object>)(result[0]);

                        if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                        {
                            wk_order.updatestatus = true;
                            wk_order.updateInfo = "呼叫AGV小车成功";
                        }

                        else
                        {
                            wk_order.updatestatus = false;
                            wk_order.updateInfo = "呼叫AGV小车失败:" + obj["ReturnMsg"].ToString();
                        }
                    }
                    if (bus.Code.Equals("CPS3001"))
                    {
                        object[] result = (object[])bus.Data.Returns;
                        //先输出信息
                        wk_order.updateInfo = _serial.Serialize(result);
                        File.WriteAllText("c:\\a.txt", wk_order.updateInfo);
                        var obj = (Dictionary<string, object>)(result[0]);
                        if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                        {
                            wk_order.updatestatus = true;
                            wk_order.updateInfo = "设备已就绪";

                        }
                        else
                        {
                            wk_order.updatestatus = false;
                            wk_order.updateInfo = "设备未就绪:" + obj["ReturnMsg"].ToString();
                        }
                    }
                }

                else if (bus.Data.Code == "Error")
                {
                    wk_order.updateInfo = bus.Data.Returns.ToString();
                }
            }
            return _serial.Serialize(wk_order);
        }

        /// <summary>
        /// 获取工单
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static WorkOrder DeserializeoObj(string Message)
        {
            if (!string.IsNullOrEmpty(Message))
            {
                return (WorkOrder)_serial.Deserialize(Message, typeof(WorkOrder));
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 返回消息
        /// </summary>
        /// <param name="Message"></param>
        /// <returns></returns>
        public static RetMessage DesRetMessage(string Message)
        {
            if (!string.IsNullOrEmpty(Message))
            {
                return (RetMessage)_serial.Deserialize(Message, typeof(RetMessage));
            }
            else
            {
                return null;
            }
        }



    }
}
