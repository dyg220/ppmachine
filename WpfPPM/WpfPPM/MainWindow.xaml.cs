﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WcsApi;
using PPMachine;
using WCS.API;
using System.Net;
using System.Web.Script.Serialization;
using WpfPPM.uc;
using NationalInstruments.Net;
using System.Xml.Linq;

namespace WpfPPM
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        private DispatcherTimer sys_timer = new DispatcherTimer(); //从ui线程启动
        System.Timers.Timer plc_get_timer;  //OPC读取定时器
        //System.Timers.Timer downLoad_timer;  //Socket服务器读取定时器
        System.Timers.Timer upLoad_timer; //Socket服务器上传定时器

        string conn_str = "服务器连接状态:{0}";
        PPDevice.SocketUrl sockect_url;
        XElement element = XmlHelper.Load("Targets.xml");


        List<DataSocket> s_read = new List<DataSocket>(11);  //参数数据
        List<DataSocket> ds_alarm = new List<DataSocket>(12); //报警数据


        DataSocketData rs_data = new DataSocketData();

        public delegate void DisplayUI(string content, int count);

        public delegate void UpDateUI(WorkOrder model, string buscode);

        public delegate void AddMessageUI(string content, string tp);

        string[] alarms = new string[] { "横切刀", "前送料辊", "气胀轴", "模组", "横切灯罩", "系统启动未完全", "无料", "冷水机", "堵料", "横切刀口危险", "横切到达总裁切次数", "纵切到达总裁切米数" };
        bool[] alarm_vals = new bool[12];

        public MainWindow()
        {
            InitializeComponent();
            double x = SystemParameters.WorkArea.Width;//得到屏幕工作区域宽度
            double y = SystemParameters.WorkArea.Height;//得到屏幕工作区域高度
            double x1 = SystemParameters.PrimaryScreenWidth;//得到屏幕整体宽度
            double y1 = SystemParameters.PrimaryScreenHeight;//得到屏幕整体高度

            WCSApi.Get += MessageRecieve;
            WCSApi.ShowNotice += NoticeRecieve;
            for (int i = 0; i < 11; i++)
            {
                s_read.Add(new DataSocket());
            }
            for (int i = 0; i < 12; i++)
            {
                ds_alarm.Add(new DataSocket());
            }

            rs_data.Attributes.Add("Hmi_Total_CrossCuttingQty", "");  //纵切刀限制米数设定
            rs_data.Attributes.Add("Hmi_TotalCuttingQty", ""); //横切刀限制次数设定
            rs_data.Attributes.Add("Hmi_MovingSpeed", "");  //送料速度
            rs_data.Attributes.Add("NowcuttingQty", "");  //当前裁切次数
            rs_data.Attributes.Add("TotalCuttingQty", "");  //当前总裁切次数
            rs_data.Attributes.Add("Length_MainMovingaxis_Total", ""); //当前总送料米数
            rs_data.Attributes.Add("Hmi_PPtype", "");  //PP料号
            rs_data.Attributes.Add("Hmi_SetCuttingQty", "");  //裁切次数
            rs_data.Attributes.Add("Hmi_CuttingLength", "");  //裁切长度
            // data.Attributes.Add("Hmi_CuttingWidth", "");  //裁切宽度,socket服务器获取
            rs_data.Attributes.Add("Qbo_Lamp", ""); //机器运行状态
            rs_data.Attributes.Add("Send_OK", ""); //Send_OK
        }
        private void SetAlarmInfo(XElement element)
        {
            for (int i = 0; i < alarms.Length; i++)
            {
                AlarmControl ac = new AlarmControl();
                ac.Name = "ac" + i.ToString();
                ac.Margin = new Thickness(6, 6, 6, 6);
                ac.textBlock1.Text = alarms[i];
                ac.led1.Value = alarm_vals[i];
                wp_alarms.Children.Add(ac);
                //gb_alarms.Content = wp_alarms.Children;
            }

            //横切刀报警
            XElement target1 = XmlHelper.QueryElements(element, "target", "name", "gbo_zonalknife_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_ZonalKnife_Alarm_Doing = XmlHelper.QueryElement(target1, "url").Value;
            ds_alarm[0].Connect(sockect_url.Url_gbo_ZonalKnife_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //前送料辊报警
            XElement target2 = XmlHelper.QueryElements(element, "target", "name", "gbo_FrontRoller_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_FrontRoller_Alarm_Doing = XmlHelper.QueryElement(target2, "url").Value;
            ds_alarm[1].Connect(sockect_url.Url_gbo_FrontRoller_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //气胀轴报警
            XElement target3 = XmlHelper.QueryElements(element, "target", "name", "gbo_AirRoller_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_AirRoller_Alarm_Doing = XmlHelper.QueryElement(target3, "url").Value;
            ds_alarm[2].Connect(sockect_url.Url_gbo_AirRoller_Alarm_Doing, AccessMode.ReadAutoUpdate);


            //模组报警
            XElement target4 = XmlHelper.QueryElements(element, "target", "name", "gbo_RightModule_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_RightModule_Alarm_Doing = XmlHelper.QueryElement(target4, "url").Value;
            ds_alarm[3].Connect(sockect_url.Url_gbo_RightModule_Alarm_Doing, AccessMode.ReadAutoUpdate);


            //横切灯罩报警
            XElement target5 = XmlHelper.QueryElements(element, "target", "name", "gbo_ZonalHeatter_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_ZonalHeatter_Alarm_Doing = XmlHelper.QueryElement(target5, "url").Value;
            ds_alarm[4].Connect(sockect_url.Url_gbo_ZonalHeatter_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //系统启动未完全报警
            XElement target6 = XmlHelper.QueryElements(element, "target", "name", "gbo_start_sign_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_Start_Sign_Alarm_Doing = XmlHelper.QueryElement(target6, "url").Value;
            ds_alarm[5].Connect(sockect_url.Url_gbo_Start_Sign_Alarm_Doing, AccessMode.ReadAutoUpdate);

            //无料报警
            XElement target7 = XmlHelper.QueryElements(element, "target", "name", "gbo_NoPP_alarm_doing").FirstOrDefault();
            sockect_url.Url_gbo_NoPP_Alarm_Doing = XmlHelper.QueryElement(target7, "url").Value;
            ds_alarm[6].Connect(sockect_url.Url_gbo_NoPP_Alarm_Doing, AccessMode.ReadAutoUpdate);


            //冷水机报警
            XElement target8 = XmlHelper.QueryElements(element, "target", "name", "gbo_Cooler_Alarm").FirstOrDefault();
            sockect_url.Url_gbo_Cooler_Alarm = XmlHelper.QueryElement(target8, "url").Value;
            ds_alarm[7].Connect(sockect_url.Url_gbo_Cooler_Alarm, AccessMode.ReadAutoUpdate);


            //堵料报警
            XElement target9 = XmlHelper.QueryElements(element, "target", "name", "gbo_PPError_Alarm").FirstOrDefault();
            sockect_url.Url_gbo_PPError_Alarm = XmlHelper.QueryElement(target9, "url").Value;
            ds_alarm[8].Connect(sockect_url.Url_gbo_PPError_Alarm, AccessMode.ReadAutoUpdate);

            //横切刀口危险报警
            XElement target10 = XmlHelper.QueryElements(element, "target", "name", "gbo_ZonalCutter_Alarm").FirstOrDefault();
            sockect_url.Url_gbo_ZonalCutter_Alarm = XmlHelper.QueryElement(target10, "url").Value;
            ds_alarm[9].Connect(sockect_url.Url_gbo_ZonalCutter_Alarm, AccessMode.ReadAutoUpdate);

            //横切到达总裁切次数报警
            XElement target11 = XmlHelper.QueryElements(element, "target", "name", "gbo_totalcuttingqty_alarm").FirstOrDefault();
            sockect_url.Url_gbo_TotalCuttingQty_Alarm = XmlHelper.QueryElement(target11, "url").Value;
            ds_alarm[10].Connect(sockect_url.Url_gbo_TotalCuttingQty_Alarm, AccessMode.ReadAutoUpdate);

            //纵切到达总裁切米数报警
            XElement target12 = XmlHelper.QueryElements(element, "target", "name", "gbo_total_crosscuttingqty_alarm").FirstOrDefault();
            sockect_url.Url_gbo_Total_CrossCuttingQty_Alarm = XmlHelper.QueryElement(target12, "url").Value;
            ds_alarm[11].Connect(sockect_url.Url_gbo_Total_CrossCuttingQty_Alarm, AccessMode.ReadAutoUpdate);



        }
        public void NoticeRecieve(string message)
        {

        }
        WorkOrder wk_order = new WorkOrder();
        public void MessageRecieve(string message)
        {
            IMesbus bus = (IMesbus)_serial.Deserialize(message, typeof(IMesbus));
            if (bus.Data.Code == "Success")
            {
                UpDateUI update = new UpDateUI(showMsg);
                AddMessageUI addmessage = new AddMessageUI(AddMeassage);
                if (bus.Code.Equals("CPS000"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.updateInfo = obj["ReturnMsg"].ToString();
                    MessageBox.Show(wk_order.updateInfo);
                }
                if (bus.Code.Equals("CPS1001"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    wk_order.TaskID = obj["TaskID"].ToString();
                    wk_order.SubTaskID = obj["SubTaskID"].ToString();
                    wk_order.CustomCode = obj["CustomCode"].ToString(); //1080
                    wk_order.PlanQuantity = int.Parse(obj["PlanQuantity"].ToString());
                    wk_order.PlanTime = float.Parse(obj["PlanTime"].ToString());
                    wk_order.LineCode = obj["LineCode"].ToString();
                    wk_order.ProcID = int.Parse(obj["ProcID"].ToString());
                    wk_order.CurrProcCode = obj["CurrProcCode"].ToString();
                    wk_order.MaterNo = obj["MaterNo"].ToString();

                    tb_TaskID.Dispatcher.Invoke(update, new object[] { wk_order, bus.Code });
                    //lab_MaterNo.Invoke(update, new object[] { wk_order, bus.Code });
                    //lv_workinfo.Invoke(addmessage, new object[] { "获取工单信息", "01" });

                }
                if (bus.Code.Equals("CPS1002"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        TaskSatus.isGet = true;
                        tb_status.Dispatcher.Invoke(addmessage, new object[] { "已领取", "isGet" });
                    }
                }
                if (bus.Code.Equals("CPS1003"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);



                }
                if (bus.Code.Equals("CPS1004"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        TaskSatus.isStart = true;
                        tb_status.Dispatcher.Invoke(addmessage, new object[] { "已开始", "isStart" });
                    }
                }

                if (bus.Code.Equals("CPS1005"))
                {
                    object[] result = (object[])bus.Data.Returns;
                    var obj = (Dictionary<string, object>)(result[0]);
                    if (int.Parse(obj["ReturnCode"].ToString()) > 0)
                    {
                        TaskSatus.isFinish = true;
                        tb_status.Dispatcher.Invoke(addmessage, new object[] { "已完成", "isFinish" });
                    }
                }


            }
            else if (bus.Data.Code == "Error")
            {
                MessageBox.Show(bus.Data.Returns.ToString());
            }

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

            lab_systime.Content = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            sys_timer.Interval = new TimeSpan(0, 0, 1);
            sys_timer.Tick += new EventHandler(sys_timer_Tick);
            sys_timer.Start();

            sb_connserver.Content = string.Format(conn_str, "未连接");

        }

        public void showMsg(WorkOrder model, string buscode)
        {
            switch (buscode)
            {
                case "CPS1001":

                    tb_TaskID.Text = model.TaskID;
                    tb_PlanQuantity.Text = model.PlanQuantity.ToString();
                    //tb_PlanTime.Text = model.PlanTime.ToString("f2");
                    //tb_ProcID.Text = model.ProcID.ToString();
                    tb_CurrProcCode.Text = model.CurrProcCode;
                    tb_CustomCode.Text = model.CustomCode;
                    tb_MaterLotNo.Text = model.MaterLotNo;
                    tb_MaterQuantity.Text = model.MaterQuantity.ToString();
                    //tb_StdManHour.Text = model.StdManHour;
                    tb_StartDate.Text = model.StartDate.ToString("HH:ss:mm");
                    tb_EndDate.Text = model.EndDate.ToString("HH:ss:mm");
                    tb_NextProcCode.Text = model.NextProcCode;
                    tb_NextMaterLotNo.Text = model.NextMaterLotNo;

                    //tb_MaterNo.Text = "PP料号：" + model.MaterNo;
                    //tb_PPLength.Text = "裁切长度:";
                    //tb_ppWidth.Text = "裁切宽度：";

                    break;
                default:
                    break;
            }



        }

        public void AddMeassage(string content, string tp)
        {
            switch (tp)
            {
                case "isGet":
                    tb_status.Text = content;
                    break;
                case "isStart":
                    tb_status.Text = content;
                    break;
                case "isFinish":
                    tb_status.Text = content;
                    break;
                default:
                    break;
            }



        }
        void sys_timer_Tick(object sender, EventArgs e)
        {
            lab_systime.Content = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            if (PPDevice.IsConnectServer)
            {
                led_connserver.Value = !led_connserver.Value;
            }

        }

        private void btn_connserver_Click(object sender, RoutedEventArgs e)
        {
            WinConfig config = new WinConfig();
            config.ShowDialog();
            if (config.isOK)
            {
                if (PPDevice.IsConnectServer)
                {
                    sb_connserver.Content = string.Format(conn_str, "已连接");
                    led_connserver.Value = true;
                    tb_DeviceNO.Text = PPDevice.DevicNO;
                    tb_Person.Text = PPDevice.Operson;
                    GetWorkOrder();
                }
                else
                {
                    sb_connserver.Content = string.Format(conn_str, "连接失败");
                    led_connserver.Value = false;
                }
            }
        }
        Random r = new Random();
        static JavaScriptSerializer _serial = new JavaScriptSerializer();
        private void GetWorkOrder()
        {
            if (PPDevice.IsConnectServer)
            {
                #region 构造iMesbus协议
                var model = new IMesbus();
                model.Code = "CPS1001"; //请求工单
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = PPDevice.DevicIP;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();
                newdic.Add("LineCode", PPDevice.DevicNO);  //cps1001

                //newdic.Add("SubTaskID", "14031");  // SubTaskID":"14031","TaskID":"0000000130", cps1002
                //newdic.Add("CompleteQuantity", "72");  //cps1003

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
                #endregion

            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult mbr = MessageBox.Show("是否退出?", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (MessageBoxResult.Yes == mbr)
            {
                if (plc_get_timer != null)
                {
                    plc_get_timer.Stop();
                    sys_timer.Stop();
                }
                if (plc_get_timer != null)
                {

                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            foreach (var item in ds_alarm)
            {
                if (item.IsConnected)
                {
                    item.SyncDisconnect(500);
                    item.Dispose();
                }
            }
            foreach (var item in s_read)
            {
                if (item.IsConnected)
                {
                    item.SyncDisconnect(500);
                    item.Dispose();
                }
            }
        }

        private void tab_main_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = tab_main.SelectedItem as TabItem;
            if (item.Header.ToString() == "报警信息")
            {
                SetAlarmInfo(element);
            }
            else if (item.Header.ToString() == "设备详细")
            {
                S_ReadsetParams(element);
            }

        }
        private void S_ReadsetParams(XElement element)
        {

            //纵切刀限制米数设定
            XElement target1 = XmlHelper.QueryElements(element, "target", "name", "hmi_total_crosscuttingqty").FirstOrDefault();
            sockect_url.Url_Hmi_Total_CrossCuttingQty = XmlHelper.QueryElement(target1, "url").Value;
            s_read[0].Connect(sockect_url.Url_Hmi_Total_CrossCuttingQty, AccessMode.ReadAutoUpdate);

            //横切刀限制次数设定
            XElement target2 = XmlHelper.QueryElements(element, "target", "name", "hmi_totalcuttingqty").FirstOrDefault();
            sockect_url.Url_Hmi_TotalCuttingQty = XmlHelper.QueryElement(target2, "url").Value;
            s_read[1].Connect(sockect_url.Url_Hmi_TotalCuttingQty, AccessMode.ReadAutoUpdate);


            //送料速度
            XElement target3 = XmlHelper.QueryElements(element, "target", "name", "hmi_movingspeed").FirstOrDefault();
            sockect_url.Url_Hmi_MovingSpeed = XmlHelper.QueryElement(target3, "url").Value;
            s_read[2].Connect(sockect_url.Url_Hmi_MovingSpeed, AccessMode.ReadAutoUpdate);
            //s4.Connect(sockect_url.Url_Hmi_SetCuttingQty, AccessMode.WriteAutoUpdate);

            //当前裁切次数
            XElement target4 = XmlHelper.QueryElements(element, "target", "name", "gbo_nowcuttingqty").FirstOrDefault();
            sockect_url.Url_NowcuttingQty = XmlHelper.QueryElement(target4, "url").Value;
            s_read[3].Connect(sockect_url.Url_NowcuttingQty, AccessMode.ReadAutoUpdate);

            //当前总裁切次数
            XElement target5 = XmlHelper.QueryElements(element, "target", "name", "gbo_totalcuttingqty").FirstOrDefault();
            sockect_url.Url_TotalCuttingQty = XmlHelper.QueryElement(target5, "url").Value;
            s_read[4].Connect(sockect_url.Url_TotalCuttingQty, AccessMode.ReadAutoUpdate);


            //当前总送料米数
            XElement target6 = XmlHelper.QueryElements(element, "target", "name", "gbl_length_MainMovingaxis_total_m").FirstOrDefault();
            sockect_url.Url_Length_MainMovingaxis_Total = XmlHelper.QueryElement(target6, "url").Value;
            s_read[5].Connect(sockect_url.Url_Length_MainMovingaxis_Total, AccessMode.ReadAutoUpdate);

            //PP料号
            XElement target7 = XmlHelper.QueryElements(element, "target", "name", "hmi_PPtype").FirstOrDefault();
            sockect_url.Url_Hmi_PPtype = XmlHelper.QueryElement(target7, "url").Value;
            s_read[6].Connect(sockect_url.Url_Hmi_PPtype, AccessMode.ReadAutoUpdate);
            s_read[10].Connect(sockect_url.Url_Hmi_PPtype, AccessMode.WriteAutoUpdate); //写PP料号


            //裁切次数
            XElement target8 = XmlHelper.QueryElements(element, "target", "name", "hmi_setcuttingqty").FirstOrDefault();
            sockect_url.Url_Hmi_SetCuttingQty = XmlHelper.QueryElement(target8, "url").Value;
            s_read[7].Connect(sockect_url.Url_Hmi_SetCuttingQty, AccessMode.ReadAutoUpdate);

            //裁切长度
            XElement target9 = XmlHelper.QueryElements(element, "target", "name", "hmi_cuttinglength").FirstOrDefault();
            sockect_url.Url_Hmi_CuttingLength = XmlHelper.QueryElement(target9, "url").Value;
            s_read[8].Connect(sockect_url.Url_Hmi_CuttingLength, AccessMode.ReadAutoUpdate);

            //机器运行状态
            XElement target10 = XmlHelper.QueryElements(element, "target", "name", "qbo_Lamp").FirstOrDefault();
            sockect_url.Url_Qbo_Lamp = XmlHelper.QueryElement(target10, "url").Value;
            s_read[9].Connect(sockect_url.Url_Qbo_Lamp, AccessMode.ReadAutoUpdate);


        }
        void tc_timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            //更新参数
            UpdateReadsetParmas();

            //更新报警
            UpdateAlarms();

        }

        private void UpdateReadsetParmas()
        {
            if (s_read[0].IsConnected)
            {
                if (s_read[0].SyncRead(500))
                {
                    //纵切刀限制米数设定
                    rs_data.Attributes["Hmi_Total_CrossCuttingQty"].Value = s_read[0].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_Hmi_Total_CrossCuttingQty.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Hmi_Total_CrossCuttingQty"].Value, 1 });
                }

            }
            if (s_read[1].IsConnected)
            {
                if (s_read[1].SyncRead(500))
                {
                    //横切刀限制次数设定
                    rs_data.Attributes["Hmi_TotalCuttingQty"].Value = s_read[1].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_Hmi_TotalCuttingQty.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Hmi_TotalCuttingQty"].Value, 2 });
                }

            }
            if (s_read[2].IsConnected)
            {
                if (s_read[2].SyncRead(500))
                {
                    //送料速度
                    rs_data.Attributes["Hmi_MovingSpeed"].Value = s_read[2].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_Hmi_MovingSpeed.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Hmi_MovingSpeed"].Value + "m/min", 3 });
                }

            }
            if (s_read[3].IsConnected)
            {
                if (s_read[3].SyncRead(500))
                {
                    //当前裁切次数
                    rs_data.Attributes["NowcuttingQty"].Value = s_read[3].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_NowcuttingQty.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["NowcuttingQty"].Value + "次", 4 });
                }

            }
            if (s_read[4].IsConnected)
            {
                if (s_read[4].SyncRead(500))
                {
                    //当前总裁切次数
                    rs_data.Attributes["TotalCuttingQty"].Value = s_read[4].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_TotalCuttingQty.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["TotalCuttingQty"].Value + "次", 5 });
                }

            }
            //Debug.WriteLine("s_read[5].IsConnected", s_read[5].IsConnected);
            if (s_read[5].IsConnected)
            {
                if (s_read[5].SyncRead(500))
                {
                    //当前总送料米数
                    rs_data.Attributes["Length_MainMovingaxis_Total"].Value = ((double)s_read[5].Data.Value).ToString("f2");
                    DisplayUI dui = new DisplayUI(showText);
                    tb_gbl_length_MainMovingaxis_total.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Length_MainMovingaxis_Total"].Value + "m", 6 });
                }

            }
            if (s_read[6].IsConnected)
            {
                if (s_read[6].SyncRead(500))
                {
                    //PP料号
                    rs_data.Attributes["Hmi_PPtype"].Value = s_read[6].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_PPtype.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Hmi_PPtype"].Value, 7 });
                    //Debug.WriteLine("PP料号:{0}", rs_data.Attributes["Hmi_PPtype"].Value);

                }

            }
            if (s_read[7].IsConnected)
            {
                if (s_read[7].SyncRead(500))
                {
                    //裁切次数
                    rs_data.Attributes["Hmi_SetCuttingQty"].Value = s_read[7].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_hmi_setcuttingqty.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Hmi_SetCuttingQty"].Value + "次", 8 });
                    //Debug.WriteLine("裁切次数:{0}", rs_data.Attributes["Hmi_SetCuttingQty"].Value);
                }

            }
            if (s_read[8].IsConnected)
            {
                if (s_read[8].SyncRead(500))
                {
                    //裁切长度
                    rs_data.Attributes["Hmi_CuttingLength"].Value = s_read[8].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    tb_Hmi_CuttingLength.Dispatcher.Invoke(dui, new object[] { rs_data.Attributes["Hmi_CuttingLength"].Value + "mm", 9 });
                }

            }

            if (s_read[9].IsConnected)
            {
                if (s_read[9].SyncRead(500))
                {
                    //机器运行状态
                    rs_data.Attributes["Qbo_Lamp"].Value = s_read[9].Data.Value.ToString();
                    DisplayUI dui = new DisplayUI(showText);
                    //string status = bool.Parse(rs_data.Attributes["Qbo_Lamp"].Value.ToString()) == true ? "Run" : "Stop";
                    object status = rs_data.Attributes["Qbo_Lamp"].Value;
                    led_Qbo_Lamp.Dispatcher.Invoke(dui, new object[] { status, 10 });
                }

            }
        }

        private void UpdateAlarms()
        {
            for (int i = 0; i < ds_alarm.Count; i++)
            {
                if (ds_alarm[i].IsConnected)
                {
                    if (ds_alarm[i].SyncRead(500))
                    {
                        alarm_vals[i] = (bool)ds_alarm[i].Data.Value;
                        wp_alarms.Dispatcher.Invoke(new System.Action(() =>
                        {
                            AlarmControl ac = wp_alarms.FindName("ac" + i.ToString()) as AlarmControl;
                            if (ac != null)
                            {
                                ac.led1.Value = alarm_vals[i];
                            }
                        }));

                    }

                }
            }

        }

        private void showText(string text, int choose)
        {
            switch (choose)
            {
                case 1:
                    //纵切刀限制米数设定
                    tb_Hmi_Total_CrossCuttingQty.Text = text;
                    break;
                case 2:
                    //横切刀限制次数设定
                    tb_Hmi_TotalCuttingQty.Text = text;
                    break;
                case 3:
                    //送料速度
                    tb_Hmi_MovingSpeed.Text = text;
                    break;
                case 4:
                    //当前裁切次数
                    tb_NowcuttingQty.Text = text;
                    break;
                case 5:
                    //当前总裁切次数
                    tb_TotalCuttingQty.Text = text;
                    break;
                case 6:
                    //当前总送料米数
                    tb_gbl_length_MainMovingaxis_total.Text = text;
                    break;
                case 7:
                    //PP料号
                    tb_PPtype.Text = text;
                    break;
                case 8:
                    //裁切次数
                    tb_hmi_setcuttingqty.Text = text;
                    break;
                case 9:
                    //裁切长度
                    tb_Hmi_CuttingLength.Text = text;
                    break;
                case 10:
                    //机器运行状态
                    led_Qbo_Lamp.Value = bool.Parse(text);
                    break;
                default:
                    break;
            }

        }

        private void btn_wkorder_get_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(wk_order.SubTaskID))
            {
                MessageBox.Show("请先获派工单信息");
                return;
            }
            else
            {
                if (PPDevice.IsConnectServer)
                {
                    var model = new IMesbus();
                    model.Code = "CPS1002";
                    model.Company = PPDevice.Company;
                    model.Id = r.Next(1, 100);
                    model.Device = PPDevice.DevicNO;
                    model.Check = PPDevice.DevicIP;
                    var param = new IMebusParam();
                    param.Param = new List<Dictionary<string, string>>();
                    var newdic = new Dictionary<string, string>();
                    newdic.Add("SubTaskID", wk_order.SubTaskID);
                    param.Param.Add(newdic);
                    model.Data = param;
                    WCSApi.Send(_serial.Serialize(model));
                }

            }
        }

        private void btn_wkorder_getParams_Click(object sender, RoutedEventArgs e)
        {
            if (PPDevice.IsConnectServer)
            {
                var model = new IMesbus();
                model.Code = "CPS1003";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = PPDevice.DevicIP;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("LineCode", wk_order.LineCode);
                newdic.Add("SubTaskID", wk_order.SubTaskID);

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
            }
        }

        private void btn_wkorder_start_Click(object sender, RoutedEventArgs e)
        {
            if (PPDevice.IsConnectServer)
            {
                var model = new IMesbus();
                model.Code = "CPS1004";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = PPDevice.DevicIP;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("SubTaskID", wk_order.SubTaskID);

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
            }
        }

        private void btn_wkorder_downparams_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(wk_order.SubTaskID))
            {
                MessageBox.Show("请先获派工单信息");
                return;
            }
            else
            {
                if (s_read[10].IsConnected)
                {
                    s_read[10].Data.Value = wk_order.MaterNo;
                    s_read[10].Update();
                    if (s_read[10].IsDataUpdated)
                    {
                        //PP料号
                        rs_data.Attributes["Hmi_PPtype"].Value = wk_order.MaterNo.ToString();

                        //Send_OK
                        DataSocket ds = new DataSocket();
                        XElement target = XmlHelper.QueryElements(element, "target", "name", "gbo_send_ok").FirstOrDefault();
                        sockect_url.Url_Send_OK = XmlHelper.QueryElement(target, "url").Value;
                        ds.Connect(sockect_url.Url_Send_OK, AccessMode.WriteAutoUpdate);
                        if (ds.IsConnected)
                        {
                            ds.Data.Value = 1;
                            ds.Update();
                            if (ds.IsDataUpdated)
                            {
                                rs_data.Attributes["Send_OK"].Value = true;
                                MessageBox.Show("下发参数成功！");
                            }
                            else
                            {
                                MessageBox.Show("下发参数失败！");
                            }
                        }
                    }

                }
            }
        }

        private void btn_wkorder_finished_Click(object sender, RoutedEventArgs e)
        {
            if (PPDevice.IsConnectServer)
            {
                var model = new IMesbus();
                model.Code = "CPS1005";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = PPDevice.DevicIP;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();

                newdic.Add("SubTaskID", wk_order.SubTaskID);
                newdic.Add("CompleteQuantity", wk_order.PlanQuantity.ToString());

                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));
            }
        }

        private void btn_wkorder_newget_Click(object sender, RoutedEventArgs e)
        {
            if (!TaskSatus.isFinish)
            {
                MessageBoxResult mbr = MessageBox.Show("加工未完成，是否重新获取工单信息?", "提示", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (MessageBoxResult.OK == mbr)
                {

                }
            }
            else
            {
                TaskSatus.isGet = false;
                TaskSatus.isStart = false;
                TaskSatus.isFinish = false;
                GetWorkOrder();

            }
        }

        private void btn_wkorder_reback_Click(object sender, RoutedEventArgs e)
        {
            if (PPDevice.IsConnectServer)
            {

                var model = new IMesbus();
                model.Code = "CPS000";
                model.Company = PPDevice.Company;
                model.Id = r.Next(1, 100);
                model.Device = PPDevice.DevicNO;
                model.Check = PPDevice.DevicIP;
                var param = new IMebusParam();
                param.Param = new List<Dictionary<string, string>>();
                var newdic = new Dictionary<string, string>();
                param.Param.Add(newdic);
                model.Data = param;
                WCSApi.Send(_serial.Serialize(model));

            }
        }

        private void tab_main_Loaded(object sender, RoutedEventArgs e)
        {
            plc_get_timer = new System.Timers.Timer();
            plc_get_timer.Interval = 1000; //每隔1秒发送一次
            plc_get_timer.Elapsed += new System.Timers.ElapsedEventHandler(tc_timer_Elapsed);
            plc_get_timer.AutoReset = true;
            plc_get_timer.Start();

        }

    }
}
