﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WCS.API
{
    [Serializable]
    public class IMesbus
    {
        public int Id { get; set; }
        public string Company { get; set; }
        public string Device { get; set; }
        public string Code { get; set; }
        public IMebusParam Data { get; set; }
        public string Check { get; set; }
    }
    [Serializable]
    public class IMebusParam
    {
        public string Code { get; set; }
        public bool Result { get; set; }
        public List<Dictionary<string, string>> Param { get; set; }
        public Object Returns { get; set; }
    }
}
