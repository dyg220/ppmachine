﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NationalInstruments.Net;
using System.IO;
using System.Threading;

namespace PPMachine
{
    public class PPDevice
    {
        static string ini_file = Thread.GetDomain().BaseDirectory + "sysconfig.ini";
        static PPDevice()
        {
            if (File.Exists(ini_file))
            {
                Operson = IniFile.ReadIniData("Person", "Name", "张三", ini_file);
                DevicIP = IniFile.ReadIniData("IP", "Name", "127.0.0.1", ini_file);
            }
        }

        /// <summary>
        /// 设备号
        /// </summary>
        public static string DevicNO { set; get; }

        /// <summary>
        /// 操作人
        /// </summary>
        public static string Operson { set; get; }
        /// <summary>
        /// 设备IP
        /// </summary>
        public static string DevicIP { set; get; }

        /// <summary>
        /// 服务器是否连接
        /// </summary>
        public static bool IsConnectServer { set; get; }

        /// <summary>
        /// OPC是否连接
        /// </summary>
        public static bool IsConnectOPC { set; get; }

        /// <summary>
        /// 设备所属公司
        /// </summary>
        public static string Company = "zhengyee";

        /// <summary>
        /// OPC地址
        /// </summary>
        public static string OPC_URL = "opc://localhost/OPC.SimaticNET";

        /// <summary>
        /// 下载参数
        /// </summary>
        public static bool download_params_ok = false;

        /// <summary>
        /// 上传参数
        /// </summary>
        public static bool upload_params_ok = false;


        public struct SocketUrl
        {

            #region 设定读取参数
            /// <summary>
            /// 纵切刀限制米数设定
            /// </summary>
            public string Url_Hmi_Total_CrossCuttingQty;

            /// <summary>
            /// 横切刀限制次数设定
            /// </summary>
            public string Url_Hmi_TotalCuttingQty;
            /// <summary>
            /// 送料速度
            /// </summary>
            public string Url_Hmi_MovingSpeed;

            /// <summary>
            /// 当前裁切次数
            /// </summary>
            public string Url_NowcuttingQty;

            /// <summary>
            /// 当前总裁切次数
            /// </summary>
            public string Url_TotalCuttingQty;

            /// <summary>
            /// 当前总送料米数
            /// </summary>
            public string Url_Length_MainMovingaxis_Total;

            /// <summary>
            /// PP料号
            /// </summary>
            public string Url_Hmi_PPtype;

            /// <summary>
            /// 裁切次数
            /// </summary>
            public string Url_Hmi_SetCuttingQty;


            /// <summary>
            /// 裁切长度
            /// </summary>
            public string Url_Hmi_CuttingLength;


            //裁切宽度
            //public string Url_Hmi_CuttingWidth;


            /// <summary>
            /// 机器运行状态
            /// </summary>
            public string Url_Qbo_Lamp;

            #endregion


            #region 报警参数
            /// <summary>
            /// 横切刀报警
            /// </summary>
            public string Url_gbo_ZonalKnife_Alarm_Doing;

            /// <summary>
            /// 前送料辊报警
            /// </summary>
            public string Url_gbo_FrontRoller_Alarm_Doing;

            /// <summary>
            /// 气胀轴报警
            /// </summary>
            public string Url_gbo_AirRoller_Alarm_Doing;


            /// <summary>
            /// 模组报警
            /// </summary>
            public string Url_gbo_RightModule_Alarm_Doing;

            /// <summary>
            /// 横切灯罩报警
            /// </summary>
            public string Url_gbo_ZonalHeatter_Alarm_Doing;

            /// <summary>
            /// 系统启动未完全报警
            /// </summary>
            public string Url_gbo_Start_Sign_Alarm_Doing;


            /// <summary>
            /// 无料报警
            /// </summary>
            public string Url_gbo_NoPP_Alarm_Doing;

            /// <summary>
            /// 冷水机报警
            /// </summary>
            public string Url_gbo_Cooler_Alarm;


            /// <summary>
            /// 堵料报警
            /// </summary>
            public string Url_gbo_PPError_Alarm;

            /// <summary>
            /// 横切刀口危险报警
            /// </summary>
            public string Url_gbo_ZonalCutter_Alarm;

            /// <summary>
            /// 横切到达总裁切次数报警
            /// </summary>
            public string Url_gbo_TotalCuttingQty_Alarm;

            /// <summary>
            /// 纵切到达总裁切米数报警
            /// </summary>
            public string Url_gbo_Total_CrossCuttingQty_Alarm;

            #endregion


            #region 异常参数


            /// <summary>
            /// 横切刀异常
            /// </summary>
            public string Url_gbo_Zonalknife_Alarm_Nodo;

            /// <summary>
            /// 前送料辊异常
            /// </summary>
            public string Url_gbo_FrontRoller_Alarm_Nodo;

            /// <summary>
            /// 气胀轴异常
            /// </summary>
            public string Url_gbo_AirRoller_Alarm_Nodo;

            /// <summary>
            /// 模组异常
            /// </summary>
            public string Url_gbo_RightModule_Alarm_Nodo;

            /// <summary>
            /// 横切灯罩异常
            /// </summary>
            public string Url_gbo_ZonalHeatter_Alarm_Nodo;


            /// <summary>
            /// 系统未启动完全异常
            /// </summary>
            public string Url_gbo_Start_Sign_Alarm_Nodo;

            /// <summary>
            /// 无料异常
            /// </summary>
            public string Url_gbo_NoPP_Alarm_Nodo;


            #endregion

            public string Url_Send_OK;

        }

    }
}
