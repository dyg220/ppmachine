﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PPMachine
{
    /// <summary>
    /// 物料信息
    /// </summary>
    public class Material
    {
        public string WorkCode { set; get; }	//呼叫方产线设备的工位编号（含EMCS和单机单元）
        public string LineCode { set; get; }		//当前设备号/产线号
        public string TaskId { set; get; }	//唯一任务号
        public TaskStyle taskStyle { set; get; }	//任务类型： 0:下料 1:上料2:返修3:下空架-上料4:下料-上空架5:上空料架 6:入库 7:出库 
        public string DeviceCode { set; get; }	//默认IMS
        public string TaskProcCode { set; get; }	//客户料号
        public string MaterLotNo { set; get; }	//物料批次号
        public string MaterQuantity { set; get; }	//呼叫物料数量
        public string PlatCode { set; get; }		//托盘编码，默认为空
        public string NextWorkCode { set; get; }		//下一步产线设备工位编号（含EMCS和单机单元），不明确的传产线编号NextLineCode(从工单取到)
        public DateTime EarlyArriveTm { set; get; }		//最早到达时间
        public DateTime LastArriveTm { set; get; }		//最晚到达时间
        public string ClientType { set; get; }		//终端类型。0:产线，1:单机单元，2:PDA
        public string CurrProcCode { set; get; }			//工序号

    }
    /// <summary>
    /// 0:下料 1:上料2:返修3:下空架-上料4:下料-上空架5:上空料架 6:入库 7:出库 
    /// </summary>
    public enum TaskStyle
    {
        ts0 = 0,
        ts1 = 1,
        ts2 = 2,
        ts3 = 3,
        ts4 = 4,
        ts5 = 5,
        ts6 = 6,
        ts7 = 7
    }
}
