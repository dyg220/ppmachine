﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PPMachine;
using WcsApi;
using System.Threading;

namespace WpfPPM
{
    /// <summary>
    /// WinConfig.xaml 的交互逻辑
    /// </summary>
    public partial class WinConfig : Window
    {
        public WinConfig()
        {
            InitializeComponent();
        }
        public bool isOK { set; get; }

        private void btn_ok_Click(object sender, RoutedEventArgs e)
        {
            string device_NO = txt_DeviceNO.Text.Trim();
            string address_IP = txt_ipAddress.Text.Trim();
            // string address_OPC = txt_OPC.Text.Trim();

            if (string.IsNullOrEmpty(device_NO))
            {
                MessageBox.Show("请设定设备号");
                return;
            }
            if (string.IsNullOrEmpty(address_IP))
            {
                MessageBox.Show("请设定服务器IP");
                return;
            }
            else
            {
                try
                {
                    PPDevice.DevicNO = device_NO;
                    //PPDevice.OPC = address_OPC;

                    WCSApi.SetKeyWord(device_NO);
                    WCSApi.Connect(address_IP);

                    if (WCSApi.IsConnected())
                    {
                        //WCSApi.Get += FrmMain.MessageRecieve;
                        //WCSApi.ShowNotice += FrmMain.NoticeRecieve;
                        PPDevice.IsConnectServer = true;
                        WCSApi.SendRegister(); //先注册
                        Thread.Sleep(1000);
                        WCSApi.SendApply();  //再验证
                        Thread.Sleep(1000);
                        if (WCSApi.IsSynced() && WCSApi.IsEncrypted())
                        {
                            PPDevice.IsConnectServer = true;
                            MessageBox.Show("服务器连接成功", "提示", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("服务器连接失败", "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                        PPDevice.IsConnectServer = false;
                    }
                }
                catch (Exception ex)
                {

                    MessageBox.Show("服务器连接失败:" + ex.Message, "提示", MessageBoxButton.OK, MessageBoxImage.Warning);
                    PPDevice.IsConnectServer = false;
                }

                //PPDevice.Socket.Connect(address_OPC);
                //if (PPDevice.Socket.IsConnected)
                //{
                //    PPDevice.IsConnectOPC = true;
                //    MessageBox.Show("OPC连接成功");
                //}
                //elsek
                //{
                //    PPDevice.IsConnectOPC = false;
                //    MessageBox.Show("OPC连接失败");
                //}

                isOK = true;
                this.Close();
            }
        }


        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
